from tkinter import *
from PIL import ImageTk, Image
import os
from tkinter import filedialog
from PIL import Image
import pickle
import numpy as np
import tensorflow as tf
from tensorflow import keras
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
model = keras.models.load_model('../ada_imaging/trained_models/crack_detection.h5')

class LabelImage:
    global img_canv
    global render

    canv_width = 800
    canv_height = 600

    def __init__(self):
        self.root = Tk()
        self.frame = Frame(self.root)
        self.root.title('Thin Film Image Crack Detection')
        self.open_image = Button(self.frame, text='Open...', command=self.open_call_back)


        self.image_label = Label(self.frame, text='Load Thin Film Image')
        self.image_label.config(font=("times", 20))

        self.canv = Canvas(self.frame, relief=RIDGE)

    def open_call_back(self):
        filename = filedialog.askopenfilename(filetypes = (("JPEG files", "*.jpg"),("All files", "*.*")))

        image = Image.open(filename)
        self.render = ImageTk.PhotoImage(self.resize_image(image))
        self.canv.itemconfigure(self.img_canv, image=self.render)
        image_height = 256
        jpgfile = Image.open(filename).convert('L')
        aspect = jpgfile.size[0] / jpgfile.size[1]
        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        pixels = np.asarray(out)
        pixels = pixels / 255
        pixels = pixels.reshape(1, pixels.shape[0], pixels.shape[1], 1)
        Y_Predict = model.predict(pixels)
        if Y_Predict[0][0] < Y_Predict[0][1]:
            self.image_label.config(text='Cracked')
            self.image_label.config(foreground="red")
        else:
            self.image_label.config(text='Non-Cracked')
            self.image_label.config(foreground="green")

    def resize_image(self, img):
        return img.resize((800, 600), Image.ANTIALIAS)

    def show_panel(self):

        self.frame.grid(row=0, column=0)
        # self.render = ImageTk.PhotoImage(self.resize_image(image))

        self.canv.config(width=800, height=600)
        self.img_canv = self.canv.create_image(0, 0, anchor=NW, image="")
        self.canv.grid(row=0, column=1, columnspan=2)

        self.image_label.grid(row=1, column=1)
        self.open_image.grid(row=1, column=0)

        self.root.mainloop()

if __name__ == "__main__":

    l = LabelImage()
    l.show_panel()
