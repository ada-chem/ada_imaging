import os
from PIL import Image

images_directory = 'D:\\Deep Learn\\Microscopic 2\\Dataset\\FP_TiOX1_20x_mag\\'
dataset_directory = 'D:\\Deep Learn\\Microscopic 2\\Dataset\\Crops_20x\\'

# Crop_Size = 100
Crop_Size = 400

for i in os.listdir(images_directory):
    if i.endswith('jpg'):
        print(i)
        jpgfile = Image.open(images_directory + i)
        width, height = jpgfile.size
        res_w = width % Crop_Size
        res_h = height % Crop_Size
        for h in range(0, height + 1 - Crop_Size- res_h, Crop_Size ):
            for w in range(0, width+1-Crop_Size-res_w, Crop_Size):
                cropped = jpgfile.crop((w, h, w+Crop_Size, h+Crop_Size))
                cropped.save(dataset_directory+os.path.splitext(i)[0]+'-'+str(h)+'-'+str(w)+'.jpg', "JPEG", quality=80,
                             optimize=True, progressive=True)
