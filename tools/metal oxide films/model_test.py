import os
from PIL import Image
import pickle
import numpy as np
from tensorflow import keras
from sklearn.metrics import roc_curve,roc_auc_score
import matplotlib.pyplot as plt
import sklearn

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

with open('Saved\\3\\X_Test.data', 'rb') as filehandle:
    # read the data as binary data stream
    X_Train = pickle.load(filehandle)

with open('Saved\\3\\Y_Test.data', 'rb') as filehandle:
    # read the data as binary data stream
    Y_Train = pickle.load(filehandle)


# Y_train = to_categorical(Y_train)
# Y_Validate = to_categorical(Y_Validate)

model = keras.models.load_model("Saved\\3\\mdl_wts-40-0.87.hdf5")
Y_Predict = model.predict_classes(X_Train)
n_mat = sklearn.metrics.confusion_matrix(Y_Train, Y_Predict)
plt.imshow(n_mat, cmap='binary', interpolation='None')
plt.show()
print(sklearn.metrics.classification_report(Y_Train, Y_Predict))
print(n_mat)
