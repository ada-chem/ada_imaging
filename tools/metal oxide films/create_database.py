import os
from PIL import Image,ImageFilter
import pickle
import numpy as np

dataset_Positive = 'D:\\Deep Learn\\Microscopic 2\\Dataset\\Sets_5x\\No Defect\\'

image_height = 50
images = list()
classes = list()
meta = list()

for i in os.listdir(dataset_Positive):
    if i.endswith('jpg'):
        print('Crack:'+str(i))
        jpgfile = Image.open(dataset_Positive + i)
        meta.append(i)
        aspect = jpgfile.size[0] / jpgfile.size[1]
        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        # out = out.filter(ImageFilter.FIND_EDGES)
        pixels = np.asarray(out)
        images.append(pixels)
        classes.append(0)
        meta.append(i)

        rotated_image = out.rotate(180)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(0)
        meta.append(i)

        # rotated_image = out.transpose(Image.FLIP_LEFT_RIGHT)
        # pixels = np.asarray(rotated_image)
        # images.append(pixels)
        # classes.append(0)
        # meta.append(i)
        #
        # rotated_image = rotated_image.rotate(180)
        # pixels = np.asarray(rotated_image)
        # images.append(pixels)
        # classes.append(0)
        # meta.append(i)

dataset_Negative = 'D:\\Deep Learn\\Microscopic 2\\Dataset\\Sets_5x\\Dust\\'

for i in os.listdir(dataset_Negative):
   if i.endswith('jpg'):
        print('Dewetting:' + str(i))
        jpgfile = Image.open(dataset_Negative + i)
        meta.append(i)
        aspect = jpgfile.size[0] / jpgfile.size[1]
        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        # out = out.filter(ImageFilter.FIND_EDGES)
        pixels = np.asarray(out)
        images.append(pixels)
        classes.append(1)

        rotated_image = out.rotate(180)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(1)
        meta.append(i)

        rotated_image = out.transpose(Image.FLIP_LEFT_RIGHT)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(1)
        meta.append(i)

        rotated_image = rotated_image.rotate(180)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(1)
        meta.append(i)

dataset_Negative = 'D:\\Deep Learn\\Microscopic 2\\Dataset\\Sets_5x\\Particle\\'

for i in os.listdir(dataset_Negative):
   if i.endswith('jpg'):
        print('Metal:' + str(i))
        jpgfile = Image.open(dataset_Negative + i)
        meta.append(i)
        aspect = jpgfile.size[0] / jpgfile.size[1]
        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        # out = out.filter(ImageFilter.FIND_EDGES)
        pixels = np.asarray(out)
        images.append(pixels)
        classes.append(2)

        rotated_image = out.rotate(180)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(2)
        meta.append(i)

        # rotated_image = out.transpose(Image.FLIP_LEFT_RIGHT)
        # pixels = np.asarray(rotated_image)
        # images.append(pixels)
        # classes.append(2)
        # meta.append(i)
        #
        # rotated_image = rotated_image.rotate(180)
        # pixels = np.asarray(rotated_image)
        # images.append(pixels)
        # classes.append(2)
        # meta.append(i)

dataset_Negative = 'D:\\Deep Learn\\Microscopic 2\\Dataset\\Sets_5x\\Crack\\'

for i in os.listdir(dataset_Negative):
   if i.endswith('jpg'):
        print('Crash:' + str(i))
        jpgfile = Image.open(dataset_Negative + i)
        meta.append(i)
        aspect = jpgfile.size[0] / jpgfile.size[1]
        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        # out = out.filter(ImageFilter.FIND_EDGES)
        pixels = np.asarray(out)
        images.append(pixels)
        classes.append(3)

        rotated_image = out.rotate(180)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(3)
        meta.append(i)

        rotated_image = out.transpose(Image.FLIP_LEFT_RIGHT)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(3)
        meta.append(i)

        rotated_image = rotated_image.rotate(180)
        pixels = np.asarray(rotated_image)
        images.append(pixels)
        classes.append(3)
        meta.append(i)

X_Train = np.asarray(images)
Y_Train = np.asarray(classes)

with open('X_Data.data', 'wb') as filehandle:
    pickle.dump(X_Train, filehandle)
with open('Y_Data.data', 'wb') as filehandle:
    pickle.dump(Y_Train, filehandle)
with open('Meta.data', 'wb') as filehandle:
    pickle.dump(meta, filehandle)