import os
from PIL import Image
import pickle
import numpy as np
import tensorflow as tf
from keras.datasets import mnist
from keras.utils import to_categorical
from keras import optimizers, regularizers
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, Dropout
from keras.callbacks import ModelCheckpoint
import random

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

with open('X_Data.data', 'rb') as filehandle:
    # read the data as binary data stream
    X_Train = pickle.load(filehandle)

with open('Y_Data.data', 'rb') as filehandle:
    # read the data as binary data stream
    Y_Train = pickle.load(filehandle)

perc_validate = 0.1
perc_test = 0.2
classes = 1

index_1 = np.where(Y_Train == 0)[0]
index_2 = np.where(Y_Train == 1)[0]
index_3 = np.where(Y_Train == 2)[0]
index_4 = np.where(Y_Train == 3)[0]

random.shuffle(index_1)
random.shuffle(index_2)
random.shuffle(index_3)
random.shuffle(index_4)

X_Validate1 = [X_Train[index, :, :] for index in index_1[:int(index_1.size*(perc_validate/classes))]]
Y_Validate1 = [Y_Train[index] for index in index_1[:int(index_1.size*(perc_validate/classes))]]

X_Test1 = [X_Train[index, :, :] for index in index_1[int(index_1.size*(perc_validate/classes))+1:
                                                    int(index_1.size*((perc_validate+perc_test)/classes))]]
Y_Test1 = [Y_Train[index] for index in index_1[int(index_1.size*(perc_validate/classes))+1:
                                              int(index_1.size*((perc_validate+perc_test)/classes))]]

X_Train1 = [X_Train[index, :, :] for index in index_1[int(index_1.size*((perc_validate+perc_test)/classes))+1:]]
Y_Train1 = [Y_Train[index] for index in index_1[int(index_1.size*((perc_validate+perc_test)/classes))+1:]]

X_Validate2 = [X_Train[index, :, :] for index in index_2[:int(index_2.size*(perc_validate/classes))]]
Y_Validate2 = [Y_Train[index] for index in index_2[:int(index_2.size*(perc_validate/classes))]]

X_Test2 = [X_Train[index, :, :] for index in index_2[int(index_2.size*(perc_validate/classes))+1:
                                                    int(index_2.size*((perc_validate+perc_test)/classes))]]
Y_Test2 = [Y_Train[index] for index in index_2[int(index_2.size*(perc_validate/classes))+1:
                                              int(index_2.size*((perc_validate+perc_test)/classes))]]

X_Train2 = [X_Train[index, :, :] for index in index_2[int(index_2.size*((perc_validate+perc_test)/classes))+1:]]
Y_Train2 = [Y_Train[index] for index in index_2[int(index_2.size*((perc_validate+perc_test)/classes))+1:]]

X_Validate3 = [X_Train[index, :, :] for index in index_3[:int(index_3.size*(perc_validate/classes))]]
Y_Validate3 = [Y_Train[index] for index in index_3[:int(index_3.size*(perc_validate/classes))]]

X_Test3 = [X_Train[index, :, :] for index in index_3[int(index_3.size*(perc_validate/classes))+1:
                                                    int(index_3.size*((perc_validate+perc_test)/classes))]]
Y_Test3 = [Y_Train[index] for index in index_3[int(index_3.size*(perc_validate/classes))+1:
                                              int(index_3.size*((perc_validate+perc_test)/classes))]]

X_Train3 = [X_Train[index, :, :] for index in index_3[int(index_3.size*((perc_validate+perc_test)/classes))+1:]]
Y_Train3 = [Y_Train[index] for index in index_3[int(index_3.size*((perc_validate+perc_test)/classes))+1:]]

X_Validate4 = [X_Train[index, :, :] for index in index_4[:int(index_4.size*(perc_validate/classes))]]
Y_Validate4 = [Y_Train[index] for index in index_4[:int(index_4.size*(perc_validate/classes))]]

X_Test4 = [X_Train[index, :, :] for index in index_4[int(index_4.size*(perc_validate/classes))+1:
                                                    int(index_4.size*((perc_validate+perc_test)/classes))]]
Y_Test4 = [Y_Train[index] for index in index_4[int(index_4.size*(perc_validate/classes))+1:
                                              int(index_4.size*((perc_validate+perc_test)/classes))]]

X_Train4 = [X_Train[index, :, :] for index in index_4[int(index_4.size*((perc_validate+perc_test)/classes))+1:]]
Y_Train4 = [Y_Train[index] for index in index_4[int(index_4.size*((perc_validate+perc_test)/classes))+1:]]
# X_Validate3 = [X_Train[index, :, :, :] for index in index_3[:int(index_3.size*(validate/classes))]]
# Y_Validate3 = [Y_Train[index] for index in index_3[:int(index_3.size*(validate/classes))]]

# X_Train3 = [X_Train[index, :, :, :] for index in index_3[int(index_3.size*(validate/classes))+1:]]
# Y_Train3 = [Y_Train[index] for index in index_3[int(index_3.size*(validate/classes))+1:]]

X_Validate1 = np.asarray(X_Validate1)
Y_Validate1 = np.asarray(Y_Validate1)
X_Validate2 = np.asarray(X_Validate2)
Y_Validate2 = np.asarray(Y_Validate2)
X_Validate3 = np.asarray(X_Validate3)
Y_Validate3 = np.asarray(Y_Validate3)
X_Validate4 = np.asarray(X_Validate4)
Y_Validate4 = np.asarray(Y_Validate4)

X_Train1 = np.asarray(X_Train1)
Y_Train1 = np.asarray(Y_Train1)
X_Train2 = np.asarray(X_Train2)
Y_Train2 = np.asarray(Y_Train2)
X_Train3 = np.asarray(X_Train3)
Y_Train3 = np.asarray(Y_Train3)
X_Train4 = np.asarray(X_Train4)
Y_Train4 = np.asarray(Y_Train4)

X_Test1 = np.asarray(X_Test1)
X_Test2 = np.asarray(X_Test2)
X_Test3 = np.asarray(X_Test3)
X_Test4 = np.asarray(X_Test4)
Y_Test1 = np.asarray(Y_Test1)
Y_Test2 = np.asarray(Y_Test2)
Y_Test3 = np.asarray(Y_Test3)
Y_Test4 = np.asarray(Y_Test4)

# X_Train3 = np.asarray(X_Train3)
# Y_Train3 = np.asarray(Y_Train3)

X_Validate = np.concatenate((X_Validate1, X_Validate2, X_Validate3, X_Validate4), axis=0)
Y_Validate = np.concatenate((Y_Validate1, Y_Validate2, Y_Validate3, Y_Validate4), axis=0)
# X_Validate = X_Validate1
# Y_Validate = Y_Validate1
X_train = np.concatenate((X_Train1, X_Train2, X_Train3, X_Train4), axis=0)
Y_train = np.concatenate((Y_Train1, Y_Train2, Y_Train3, Y_Train4), axis=0)

X_test = np.concatenate((X_Test1, X_Test2, X_Test3, X_Test4), axis=0)
Y_test = np.concatenate((Y_Test1, Y_Test2, Y_Test3, Y_Test4), axis=0)

X_train = X_train.reshape(X_train.shape[0], X_train.shape[1], X_train.shape[2], 3)
X_Validate = X_Validate.reshape(X_Validate.shape[0], X_Validate.shape[1], X_Validate.shape[2], 3)
X_test = X_test.reshape(X_test.shape[0], X_test.shape[1], X_test.shape[2], 3)

X_train = X_train/255
X_Validate = X_Validate/255
X_test = X_test/255

with open('X_train.data', 'wb') as filehandle:
    pickle.dump(X_train, filehandle)
with open('Y_train.data', 'wb') as filehandle:
    pickle.dump(Y_train, filehandle)
with open('X_Validate.data', 'wb') as filehandle:
    pickle.dump(X_Validate, filehandle)
with open('Y_Validate.data', 'wb') as filehandle:
    pickle.dump(Y_Validate, filehandle)
with open('X_test.data', 'wb') as filehandle:
    pickle.dump(X_test, filehandle)
with open('Y_test.data', 'wb') as filehandle:
    pickle.dump(Y_test, filehandle)

Y_train = to_categorical(Y_train)
Y_Validate = to_categorical(Y_Validate)

model = Sequential()

model.add(Conv2D(32, kernel_size=(3, 3), strides=(1, 1), activation='relu',
                 kernel_regularizer=regularizers.l2(0.001),
                 input_shape=(X_train.shape[1],
                 X_train.shape[2], 3)))
model.add(Conv2D(32, kernel_size=(3, 3), strides=(1, 1), activation='relu', kernel_regularizer=regularizers.l2(0.001)))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(32, kernel_size=(3, 3), strides=(1, 1), activation='relu', kernel_regularizer=regularizers.l2(0.001)))
model.add(Conv2D(32, kernel_size=(3, 3), strides=(1, 1), activation='relu', kernel_regularizer=regularizers.l2(0.001)))


model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
# model.add(Dropout(0.5))
# model.add(Conv2D(20, kernel_size=(1, 1), strides=(1, 1), activation='relu'))
# model.add(Conv2D(20, kernel_size=(1, 1), strides=(1, 1), activation='relu'))
# model.add(Conv2D(2, kernel_size=(61, 82), strides=(1, 1), activation='softmax'))
# model.add(MaxPooling2D(pool_size=(61, 82), strides=(1, 1)))

model.add(Flatten())
model.add(Dense(50, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(50, activation='relu'))
model.add(Dropout(0.5))
# model.add(Conv2D(50, kernel_size=(1, 1), strides=(1, 1), activation='relu', kernel_regularizer=regularizers.l2(0.001),
#                 ))
# model.add(MaxPooling2D(pool_size=(254, 339), strides=(1, 1)))


# model.add(Dropout(0.5))
model.add(Dense(4, activation='softmax'))

mcp_save = ModelCheckpoint('mdl_wts-{epoch:02d}-{val_acc:.2f}.hdf5', verbose=1, save_weights_only=False,
                           save_best_only=True, monitor='val_acc', mode='max')
callbacks_list = [mcp_save]

# sgd = optimizers.SGD(lr=0.01, momentum=0.0001, nesterov=True)
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()
# model.compile(optimizer=sgd, loss='mean_squared_error', metrics=['accuracy'])

model.fit(X_train, Y_train, validation_data=(X_Validate, Y_Validate), epochs=40, batch_size=200, shuffle=True,
          callbacks=callbacks_list)

# model.save('new_model1.h5')