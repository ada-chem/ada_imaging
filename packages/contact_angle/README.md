## Installing

```
pip install "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.contact_angle&subdirectory=packages/contact_angle""
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.contact_angle&subdirectory=packages/contact_angle"
```
