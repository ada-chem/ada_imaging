from PIL import Image,ImageDraw, ImageFont
from scipy.ndimage import filters
from scipy.signal import find_peaks
from scipy.stats import linregress
import numpy as np
from matplotlib import pyplot as plt
import os


images_dir = 'D:\\ContactAngleMeasurement\\ImageSet\\02-01-2018\\'
file_name = 'TM-1-041-pXRVB-2-3-08-06 PM-hour15-minutes08-seconds06.png'

lst = sorted(os.listdir(images_dir), key=len)
red_diff = list()

w = 160
Crop_SizeWB = 50
Crop_Sizew = 300

h = 200
Crop_Sizeh = 150

for i in lst:
    if i.endswith('png'):
        print(str(i))
        jpgfile = Image.open(images_dir + i).convert('L')
        backgroundim = jpgfile.crop((w, h, w + Crop_SizeWB, h + Crop_Sizeh))
        jpgfile = jpgfile.crop((w, h, w + Crop_Sizew, h + Crop_Sizeh))
        # red, green, blue = jpgfile.split()
        im = np.array(backgroundim, dtype='int')
        print(im.shape)

        imx = np.zeros(im.shape)
        imy = np.zeros(im.shape)
        filters.sobel(im, 1, imx)
        filters.sobel(im, 0, imy)
        magnitude = np.sqrt(imx ** 2 + imy ** 2)
        magnitude = filters.gaussian_filter(magnitude, sigma=1)
        magnitude = magnitude > np.amax(magnitude)/4
        maxIndex = list()
        print(magnitude.shape)

        for j in range(magnitude.shape[1]):
            a=magnitude[:, j];
            result = np.where(a == 1)
            if result[0].size != 0:
                maxIndex.append(result[0][0])

        maxIndex=np.asarray(maxIndex)
        HLine = int(np.median(maxIndex))
        print(HLine)

        im = np.array(jpgfile, dtype='int')
        imx = np.zeros(im.shape)
        imy = np.zeros(im.shape)
        filters.sobel(im, 1, imx)
        filters.sobel(im, 0, imy)
        magnitude = np.sqrt(imx ** 2 + imy ** 2)
        magnitude = filters.gaussian_filter(magnitude, sigma=1)
        # magnitude = magnitude > np.amax(magnitude) / 5
        MaxLineTolerance = 10
        maxIndex = list()
        xIndex=np.asarray(range(HLine-MaxLineTolerance,HLine-3))
        for j in xIndex:
            signal = magnitude[j, :]
            peaks, _ = find_peaks(signal, height=np.amax(signal) / 2)
            if peaks.size != 0:
                # print(peaks[0])
                maxIndex.append(peaks[0])
        maxIndex = np.asarray(maxIndex)

        scale=4
        im_resized = jpgfile.resize((jpgfile.size[0]*scale,jpgfile.size[1]*scale), Image.ANTIALIAS).convert('RGB')
        draw = ImageDraw.Draw(im_resized)
        draw.line((0, HLine*scale, im_resized.size[0] , HLine*scale), fill=255, width=10)

        slope, intercept, r_value, p_value, std_err = linregress(np.flip(maxIndex, 0), xIndex)
        print(slope)
        print(intercept)
        angle = np.rad2deg(np.arctan(slope))
        if angle < 0:
            angle=angle+180
        print(angle)
        AngleY = 80*scale

        draw.line((maxIndex[-1]*scale, HLine*scale, ((HLine*scale+AngleY)-(intercept*scale))/slope, HLine*scale-AngleY), fill=255, width=5)
        font = ImageFont.truetype("arial", 60)
        draw.text((10, 10), '%.2f' % angle, fill=(255, 0, 100), font=font)
        im = np.array(im_resized, dtype='int')

        plt.subplot(1, 2, 1)
        plt.imshow(magnitude, cmap='gray')
        plt.title('Gradient Magnitude')
        plt.axis('off')
        plt.subplot(1, 2, 2)
        plt.imshow(im, cmap='gray')
        plt.title('Original')
        plt.axis('off')
        fig, ax1 = plt.subplots(1, 1)
        signal = magnitude[HLine-1, :]
        peaks, _ = find_peaks(signal, height=np.amax(signal) / 2)
        ax1.plot(signal)
        ax1.plot(peaks, signal[peaks], "x")
        plt.show()
        # break
