import os
import pandas as pd
import numpy as np

# Nina's model
from PIL import Image
import pickle

from timeit import default_timer as timer

try:
    from .pipelines import segment_color2d_slic_features_model_graphcut
except:

    try:
        from pipelines import segment_color2d_slic_features_model_graphcut
    except:

        try:
            from packages.nina_segmentation.ada_imaging.nina_segmentation.pipelines import \
                segment_color2d_slic_features_model_graphcut
        except:

            pass

# plotting
import matplotlib.pyplot as plt


pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)


def predict_image_labels(image_directory, plot=False, model='classif_5_2.pkl'):
    segmentation_directory = os.path.dirname(__file__)
    model_file = os.path.join(segmentation_directory, model)

    with open(model_file, 'rb') as input:
        classif = pickle.load(input)

    img_pillow = Image.open(image_directory)

    if model=='classif_4.pkl':
        width, height = img_pillow.size
        img = np.array(img_pillow.resize((int(width / 2), int(height / 2))))

    else:
        img = np.array(img_pillow)

    sp_size = 25
    sp_regul = 0.2
    dict_features = {'color': ['mean', 'std', 'median'], 'tLM': ['mean']}

    seg, _ = segment_color2d_slic_features_model_graphcut(img, classif, sp_size=sp_size, sp_regul=sp_regul,
                                                          gc_regul=-1., dict_features=dict_features,
                                                          gc_edge_type='model')

    if plot:
        FIG_SIZE = (8. * np.array(img.shape[:2]) / np.max(img.shape))[::-1]
        fig = plt.figure(figsize=FIG_SIZE)
        plt.imshow(img)
        plt.imshow(seg, alpha=0.6, cmap=plt.cm.jet, vmin=0, vmax=5)
        _ = plt.contour(seg, levels=np.unique(seg), colors='w')
        plt.show()

    return seg


if __name__ == "__main__":

    samples = ["{0:0=3d}".format(i) for i in range(0, 10)]
    images = [rf'/Users/teddyhaley/Desktop/2020-10-05_11-19-45/sample_{sample}/XRF/000-xrf_10X-pos_0.bmp' for sample in
              samples]

    start = timer()

    predict_image_labels(images[0], model='classif_4.pkl')

    end = timer()
    print(end - start)
