from ada_imaging.ada_imaging import labelling
import os

# Use for first time only
L = labelling.LabelImage('continuous_crack_detection')

# Use for every predict, OUTPUTS: 0 for non-crack and 1 for cracked, elapsed time, probability on class
predict = L.label_file(os.path.join(os.getcwd(), 'test_image.jpg'))
print(predict)



