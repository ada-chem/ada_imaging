import os
import logging
import datetime
from PIL import Image, ImageFilter
import numpy as np
import warnings
# ignore tensorflow warnings
warnings.filterwarnings('ignore', module=".*tensorflow.*")
import tensorflow as tf
from tensorflow.python.keras.backend import set_session

package_directory = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger(__name__)

model_paths = {
    'crack': 'trained_models/crack.hdf5',
    'dewetting': 'trained_models/dewetting.hdf5',
}


class ImageClassifier:
    def __init__(self):
        # setup logging
        self.logger = logger.getChild(self.__class__.__name__)
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

        self.models = {
            'crack': {
                'file': 'trained_models/crack.hdf5',
            },
            'dewetting': {
                'file': 'trained_models/dewetting.hdf5',
            },
            'quality': {
                'file': 'trained_models/QualityModel.hdf5',
            },
        }

        # load all the models
        self._model_setup()

    def _model_setup(self):
        for name, d in self.models.items():
            self.models[name]['path'] = os.path.join(os.path.dirname(os.path.abspath(__file__)), d['file'])
            self.models[name]['session'] = tf.compat.v1.Session(graph=tf.Graph())

    def _crack(self, jpg, crop_size=100, image_height=600):
        """image preparation"""
        aspect = jpg.size[0] / jpg.size[1]
        out = jpg.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        out = out.filter(ImageFilter.FIND_EDGES)
        width, height = out.size
        res_w = width % crop_size
        res_h = height % crop_size
        images = list()
        for h in range(0, height + 1 - crop_size, crop_size - res_h):
            for w in range(0, width + 1 - crop_size, crop_size - res_w):
                cropped = out.crop((w, h, w + crop_size, h + crop_size))
                pixels = np.asarray(cropped)
                images.append(pixels)
        return images

    def _dewetting(self, jpg, crop_size=50, image_height=1200):
        """image preparation"""
        aspect = jpg.size[0] / jpg.size[1]
        out = jpg.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        width, height = out.size
        res_w = width % crop_size
        res_h = height % crop_size
        images = list()
        for h in range(0, height + 1 - crop_size, crop_size - res_h):
            for w in range(2 * crop_size, width + 1 - crop_size, crop_size - res_w):
                cropped = out.crop((w, h, w + crop_size, h + crop_size))
                pixels = np.asarray(cropped)
                images.append(pixels)
        return images

    def _quality(self, jpg, crop_size=50, image_height=400):
        """image preparation"""
        aspect = jpg.size[0] / jpg.size[1]
        out = jpg.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        # out = out.filter(ImageFilter.FIND_EDGES)
        width, height = out.size
        res_w = width % crop_size
        res_h = height % crop_size
        images = list()
        for h in range(0, height + 1 - crop_size, crop_size - res_h):
            for w in range(2 * crop_size, width + 1 - crop_size, crop_size - res_w):
                cropped = out.crop((w, h, w + crop_size, h + crop_size))
                pixels = np.asarray(cropped)
                images.append(pixels)
        return images

    def label_image(self, model, image_file):

        jpgfile = Image.open(image_file).convert('L')

        # get the prep for that image file
        images = getattr(self, f"_{model}")(jpgfile)

        x_train = np.asarray(images)
        x_train = x_train / 255
        x_train = x_train.reshape(x_train.shape[0], x_train.shape[1], x_train.shape[2], 1)
        a = datetime.datetime.now()

        # predict on image
        y_predict = self._predict(model, x_train)

        b = datetime.datetime.now()
        s = 0
        for p in y_predict[:, 0]:
            s += p
        score = s / x_train.shape[0]
        delta = b - a

        return score, delta

    def _predict(self, name, data):
        # use the model
        session = self.models[name]['session']
        with session.graph.as_default():
            set_session(session)
            model = tf.keras.models.load_model(self.models[name]['path'])
            return model.predict(data)

    def __del__(self):
        self.logger.info("clearing keras session")
        tf.keras.backend.clear_session()


class LabelImage:
    def __init__(self, model_name):

        self.logger = logger.getChild(self.__class__.__name__)
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

        # Load the model into the class instance
        self.model_name = model_name
        path = os.path.join(package_directory, model_paths[model_name])
        self.model = tf.keras.models.load_model(path)

    def __del__(self):
        self.logger.info("clearing keras session in LabelImage destructor")
        tf.keras.backend.clear_session()

    def label_file(self, file_address):

        if self.model_name == 'crack':
            return self.label_crack(file_address)

        if self.model_name == 'dewetting':
            return self.label_dewetting(file_address)

    def label_crack(self, file_address):
        jpgfile = Image.open(file_address).convert('L')
        crop_size = 100
        image_height = 600
        aspect = jpgfile.size[0] / jpgfile.size[1]

        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        out = out.filter(ImageFilter.FIND_EDGES)
        width, height = out.size
        res_w = width % crop_size
        res_h = height % crop_size
        images = list()
        for h in range(0, height + 1 - crop_size, crop_size - res_h):
            for w in range(0, width + 1 - crop_size, crop_size - res_w):
                cropped = out.crop((w, h, w + crop_size, h + crop_size))
                pixels = np.asarray(cropped)
                images.append(pixels)

        X_Train = np.asarray(images)
        X_Train = X_Train / 255
        X_Train = X_Train.reshape(X_Train.shape[0], X_Train.shape[1], X_Train.shape[2], 1)
        a = datetime.datetime.now()
        Y_Predict = self.model.predict(X_Train)
        b = datetime.datetime.now()
        sum = 0
        for p in Y_Predict[:, 0]:
            sum += p
        score = sum / X_Train.shape[0]
        delta = b - a

        return score, delta

    def label_dewetting(self, file_address):
        jpgfile = Image.open(file_address).convert('L')
        crop_size = 50
        image_height = 1200
        aspect = jpgfile.size[0] / jpgfile.size[1]

        out = jpgfile.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)
        width, height = out.size
        res_w = width % crop_size
        res_h = height % crop_size
        images = list()
        for h in range(0, height + 1 - crop_size, crop_size - res_h):
            for w in range(2 * crop_size, width + 1 - crop_size, crop_size - res_w):
                cropped = out.crop((w, h, w + crop_size, h + crop_size))
                pixels = np.asarray(cropped)
                images.append(pixels)

        X_Train = np.asarray(images)
        X_Train = X_Train / 255
        X_Train = X_Train.reshape(X_Train.shape[0], X_Train.shape[1], X_Train.shape[2], 1)
        a = datetime.datetime.now()
        Y_Predict = self.model.predict(X_Train)
        b = datetime.datetime.now()
        sum = 0

        for p in Y_Predict[:, 0]:
            sum += p
        score = sum / X_Train.shape[0]
        delta = b - a

        return score, delta


if __name__ == "__main__":

    ic = ImageClassifier()
    print(ic.label_image('crack', "test_image.jpg"))
    print(ic.label_image('dewetting', "test_image.jpg"))
    print(ic.label_image('quality', "test_image.jpg"))


    li = LabelImage("crack")
    z = li.label_file("test_image.jpg")
    print(f"continuous_crack_detection test_image.jpg result: {z}")

    # need to delete
    del li

    li = LabelImage("dewetting")
    z = li.label_file("test_image.jpg")
    print(f"dewtting test_image.jpg result: {z}")
