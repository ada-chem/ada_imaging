## Installing

```
pip install "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_homogen&subdirectory=packages/film_homogen"
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_homogen&subdirectory=packages/film_homogen"
```
