import os
import pandas as pd
import numpy as np
import json
from pprint import pprint
import collections

from skimage.util import img_as_float
from skimage.util import img_as_ubyte
from skimage import io
from skimage.color import rgb2gray, gray2rgb
from skimage.segmentation import felzenszwalb
import mahotas.features.texture as mht

from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN

import matplotlib.pyplot as plt
import plotly.express as px

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)


def plot_homogeneity(df, col1, col2, color, label):
    x = df[col1]
    y = df[col2]
    c = df[color]
    plt.scatter(x=x, y=y, c=c, s=30, cmap='viridis', alpha=0.5)
    cb = plt.colorbar()
    cb.set_label(color)
    plt.xlabel(col1)
    plt.ylabel(col2)

    for i in range(0, len(x)):
        plt.annotate(df[label][i],
                     (x[i], y[i]),
                     textcoords="offset points",
                     xytext=(0, 10),
                     ha='center')

    plt.show()


def homogeneity_data_generation(campaign_directory):
    parent_directory = os.path.dirname(campaign_directory)

    sample_agg = pd.read_csv(
        os.path.join(parent_directory, 'Processed', 'data_pipeline', 'processed_data', 'sample_aggregate.csv'))
    sample_agg['homogeneity'] = 0

    conductance_df = pd.read_csv(
        os.path.join(parent_directory, 'Processed', 'data_pipeline', 'processed_data', "position_conductivity.csv"))

    campaign_json_dir = os.path.join(campaign_directory, "config.json")
    with open(campaign_json_dir, 'r') as json_data:
        campaign_data = json.load(json_data)

    reference_scale = {
        'darkfield_px_mm': 3000 / 25,  # 3000 pixels / 25mm
        'lightfield_px_mm': 117 / 3,  # 117 pixels / 3mm
        'lightfield_x_offset_mm': 1.40,
        'lightfield_y_offset_mm': -2.15,
    }

    probe_locations = campaign_data['SPECTROMETRY']['OFFSETS']
    probe_locations.reverse()

    sample_folder = [os.path.join(campaign_directory, sample) for sample in os.listdir(campaign_directory) if
                     os.path.isdir(os.path.join(campaign_directory, sample)) and sample.startswith('sample')]

    for sample_dir in sample_folder:
        print(sample_dir)
        sample_number = int(os.path.basename(sample_dir).split('_')[1])
        image_dir = os.path.join(sample_dir, 'XRF', '005-xrf_10X-pos_0.bmp')
        raw_image = io.imread(image_dir)
        segment_map = fw_image_segmentation(image_directory=image_dir)

        raw_image_copy = raw_image.copy()
        masks = probe_mapping(image_matrix=raw_image_copy,
                              probe_locations=probe_locations,
                              scale_px_mm=reference_scale['lightfield_px_mm'],
                              x_offset_mm=reference_scale['lightfield_x_offset_mm'],
                              y_offset_mm=reference_scale['lightfield_y_offset_mm'],
                              probe_x_mm=0.5,
                              probe_y_mm=3.0,
                              plot=False,
                              rotate90=0)

        homogen = homogeneity_score(image_matrix=segment_map,
                                    masks=masks)

        sample_agg.loc[sample_agg['sample'] == sample_number, ['homogeneity']] = homogen

    print(sample_agg)
    sample_agg.to_csv(os.path.join(parent_directory, 'samples_homogen.csv'))
    fig = px.scatter(sample_agg,
                     x="fuel_oxidizer_ratio_realized",
                     y="temperature_realized",
                     color="homogeneity")
    fig.show()


def homogeneity_score(image_matrix: np.ndarray,
                      masks: list):
    occurances_list = []
    for position_number, mask in enumerate(masks):
        bool_mask = mask == 1
        occurances_list = occurances_list + image_matrix[bool_mask].tolist()

    occurance_dict = dict(collections.Counter(occurances_list))
    pprint(occurance_dict)
    most_common_key = list(occurance_dict.keys())[0]
    largest_area = occurance_dict[most_common_key]
    total_area = sum(occurance_dict.values())
    homogen = largest_area / total_area
    print(homogen)
    return homogen


def draw_probe_location(image_matrix: np.ndarray,
                        masks: list,
                        conductance_df: pd.DataFrame,
                        sample_number: int,
                        plot: bool = False):
    try:
        image_matrix = gray2rgb(image_matrix)
    except:
        pass

    sample_conductance_df = conductance_df[conductance_df['sample'] == sample_number]

    for position_number, mask in enumerate(masks):
        position_conductance_df = sample_conductance_df[sample_conductance_df['position'] == position_number]
        position_conductance = position_conductance_df.iloc[0]['conductance']

        mask_color = [255, 0, 0]

        if position_conductance > 0:
            mask_color = [0, 255, 0]

        image_matrix[np.nonzero(mask)] = mask_color

    if plot:
        plt.imshow(image_matrix)
        plt.show()

    return image_matrix


def probe_mapping(image_matrix: np.ndarray,
                  probe_locations: list,
                  scale_px_mm: float,
                  reference_box_margin: int = 10,
                  x_offset_mm: float = 0.0,
                  y_offset_mm: float = 0.0,
                  probe_x_mm: float = 3.0,
                  probe_y_mm: float = 0.5,
                  rotate90: int = 0,
                  plot=False):
    for x in range(rotate90):
        image_matrix = np.rot90(image_matrix)

    # Use middle of image as reference
    x = image_matrix.shape[0]
    y = image_matrix.shape[1]
    x_ref = x // 2
    y_ref = y // 2

    # Correct for offset
    x_correction = int(scale_px_mm * x_offset_mm)
    y_correction = int(scale_px_mm * y_offset_mm)

    x_corrected = x_ref + x_correction
    y_corrected = y_ref + y_correction

    # plot the reference points to confirm offset
    if plot:
        matrix_copy = image_matrix.copy()

        # Place center marker
        matrix_copy[x_ref - reference_box_margin: x_ref + reference_box_margin,
        y_ref - reference_box_margin: y_ref + reference_box_margin] = [255, 0, 0]

        matrix_copy[x_corrected - reference_box_margin: x_corrected + reference_box_margin,
        y_corrected - reference_box_margin: y_corrected + reference_box_margin] = [0, 0,
                                                                                   255]
        plt.imshow(matrix_copy)
        plt.show()

    masks = []

    for position_number, mapping in enumerate(probe_locations):

        x_probe_center_uncorrected = mapping['x']
        y_probe_center_uncorrected = mapping['y']

        x_probe_center = int(x_corrected + (x_probe_center_uncorrected * scale_px_mm))
        y_probe_center = int(y_corrected + (y_probe_center_uncorrected * scale_px_mm))

        mask_matrix = np.zeros_like(rgb2gray(image_matrix))

        probe_x_margin = int((probe_x_mm * scale_px_mm) // 2)
        probe_y_margin = int((probe_y_mm * scale_px_mm) // 2)

        mask_matrix[x_probe_center - probe_x_margin: x_probe_center + probe_x_margin,
        y_probe_center - probe_y_margin: y_probe_center + probe_y_margin] = 1

        masks.append(mask_matrix)

        if plot:
            probe_matrix = image_matrix.copy()
            print('Position Number: ', position_number)
            probe_matrix[x_probe_center - probe_x_margin: x_probe_center + probe_x_margin,
            y_probe_center - probe_y_margin: y_probe_center + probe_y_margin] = [0, 0, 255]
            plt.imshow(probe_matrix)
            plt.show()

    return masks


def fw_image_segmentation(image_directory, scale=50, sigma=3, min_size=50, eps=0.9, plot=False):
    image = io.imread(image_directory)

    # Convert image to float
    image = img_as_float(image)
    image_grey = rgb2gray(image)

    # Reserve 0 for mask mapping. Will make GLCM possible because of irregularly shaped regions.
    image_grey = img_as_ubyte(image_grey)
    image_grey[image_grey == 0] = 1

    # felzenszwalb segmentation
    segments_fz = felzenszwalb(image, scale=scale, sigma=sigma, min_size=min_size)
    unique_segments = np.unique(segments_fz)

    # Convert segment to bitmap
    segment_list = []
    for segment_number in unique_segments:
        segment = (segments_fz == segment_number).astype(int)
        segment_list.append((segment, segment_number))

    segment_textures = []
    for segment_tuple in segment_list:
        segment = segment_tuple[0]
        segment_number = segment_tuple[1]

        # Use the segment to mask the grey image to do haralick texture feature analysis
        masked_image_grey = segment * image_grey

        bool_mask = segment == 1

        r = np.median(image[bool_mask, 0])
        g = np.median(image[bool_mask, 1])
        b = np.median(image[bool_mask, 2])

        try:
            features = mht.haralick(masked_image_grey,
                                    ignore_zeros=True,
                                    return_mean=True)
        except:
            features = [np.nan for i in range(0, 13)]

        segment_dict = {
            'segment_number': segment_number,
            'feature_14': r,
            'feature_15': g,
            'feature_16': b
        }

        for feature_number, feature in enumerate(features):
            segment_dict[f"feature_{feature_number}"] = feature

        segment_textures.append(segment_dict)

    features_df = pd.DataFrame(segment_textures)
    features_df.fillna(features_df.mean(), inplace=True)

    X = features_df.loc[:, features_df.columns.str.contains('^feature')].values

    # Standardizing the features
    X = StandardScaler().fit_transform(X)
    db = DBSCAN(eps=eps, min_samples=1, n_jobs=-1).fit(X)

    features_df['label'] = db.labels_ + 1

    # Segment Map
    segment_map = np.zeros_like(image_grey)
    for segment_tuple in segment_list:
        segment = segment_tuple[0]
        segment_number = segment_tuple[1]
        segment_bool = np.array(segment, dtype=bool)
        label_number = features_df[features_df['segment_number'] == segment_number]['label']
        segment_map[segment_bool] = label_number

    if plot:
        plt.imshow(segment_map)
        plt.title(f"Number of segments: {len(np.unique(features_df['label']))}")
        plt.show()

    return segment_map


def image_segmentation(image_directory, eps=0.5, segments=100, metric='euclidean', min_samples=5, plot=False):
    image = io.imread(image_directory)
    image_grey = rgb2gray(image)
    image_grey = img_as_ubyte(image_grey)

    M = image_grey.shape[0] // segments
    N = image_grey.shape[1] // segments

    tiles = [image[x:x + M, y:y + N] for x in range(0, image.shape[0], M) for y in
             range(0, image.shape[1], N)]

    segment_features = []
    for tile_number, tile in enumerate(tiles):

        r = np.median(tile[:, :, 0])
        g = np.median(tile[:, :, 1])
        b = np.median(tile[:, :, 2])
        tile = img_as_ubyte(rgb2gray(tile))
        features = mht.haralick(tile, return_mean=True)
        segment_feature = {
            'tile_number': tile_number,
            'feature_14': r,
            'feature_15': g,
            'feature_16': b
        }

        for feature_number, feature in enumerate(features):
            segment_feature[f"feature_{feature_number}"] = feature

        segment_features.append(segment_feature)

    segment_df = pd.DataFrame(segment_features)

    X = segment_df.loc[:, segment_df.columns.str.contains('^feature')].values

    # Standardizing the features
    X = StandardScaler().fit_transform(X)
    db = DBSCAN(eps=eps, metric=metric, n_jobs=-1, min_samples=min_samples).fit(X)
    # db = OPTICS().fit(X)

    segment_df['label'] = db.labels_

    segment_map = np.zeros_like(image_grey)
    segment = 0
    for x in range(0, image_grey.shape[0], M):
        for y in range(0, image_grey.shape[1], N):
            segment_label = segment_df[segment_df['tile_number'] == segment]['label'] + 1
            segment_map[x:x + M, y:y + N] = segment_label
            segment = segment + 1

    if plot:
        plt.imshow(segment_map)
        plt.show()

    return segment_map


if __name__ == "__main__":
    # #######################################
    # ### Tile Image Segmentation Example ###
    # min_samples = [5]
    # epss = [0.5]
    # segments = [50]
    # metrics = ['euclidean']
    # samples = ['017']
    #
    # for eps in epss:
    #     for segment in segments:
    #         for metric in metrics:
    #             for sample in samples:
    #                 for min_sample in min_samples:
    #                     print(sample, min_sample, eps)
    #                     image_segmentation(
    #                         image_directory=rf"C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\2020-09-22_17-24-46\sample_{sample}\XRF\005-xrf_10X-pos_0.bmp",
    #                         segments=segment,
    #                         eps=eps,
    #                         min_samples=min_sample
    #                     )

    # #####################################
    # ### FW Image Segmentation Example ###
    # scales = [50]
    # sigmas = [3]
    # min_sizes = [50]
    # epss = [0.9]
    # samples = ['004', '005', '017']
    #
    # for scale in scales:
    #     for sigma in sigmas:
    #         for min_size in min_sizes:
    #             for eps in epss:
    #                 for sample in samples:
    #                     print(scale, sigma, min_size, eps)
    #                     fw_image_segmentation(
    #                         image_directory=rf"C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\2020-09-22_17-24-46\sample_{sample}\XRF\005-xrf_10X-pos_0.bmp",
    #                         scale=scale,
    #                         sigma=sigma,
    #                         min_size=min_size,
    #                         eps=eps,
    #                         plot=True)

    # #############################
    # ### Probe Mapping Example ###
    # with open(r"C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\2020-09-22_17-24-46\config.json",
    #           'r') as json_data:
    #     data = json.load(json_data)
    #
    # # Getting probe locations from the config.json
    # probe_locations = data['SPECTROMETRY']['OFFSETS']
    # probe_locations.reverse()
    #
    # reference_scale = {
    #     'darkfield_px_mm': 3000 / 25,  # 3000 pixels / 25mm
    #     'lightfield_px_mm': 221 / 3,
    #     'lightfield_x_offset_mm': 0.95,
    #     'lightfield_y_offset_mm': -1.85,
    #
    # }
    #
    # image_directory = r"C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\2020-09-22_17-24-46\sample_017\XRF\005-xrf_10X-pos_0.bmp"
    # image = io.imread(image_directory)
    # masks = probe_mapping(image_matrix=image,
    #                       probe_locations=probe_locations,
    #                       scale_px_mm=reference_scale['lightfield_px_mm'],
    #                       x_offset_mm=reference_scale['lightfield_x_offset_mm'],
    #                       y_offset_mm=reference_scale['lightfield_y_offset_mm'],
    #                       probe_x_mm=0.5,
    #                       probe_y_mm=3.0,
    #                       plot=True,
    #                       rotate90=0)
    #
    # conductance_df = pd.read_csv(
    #     r"C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\Processed\data_pipeline\processed_data\position_conductivity.csv")
    # draw_probe_location(image_matrix=image,
    #                     masks=masks,
    #                     conductance_df=conductance_df,
    #                     sample_number=17)

    # ################################
    # ### homogenity score example ###
    # campaign_dir = r'C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\2020-09-22_17-24-46'
    # image_dir = os.path.join(campaign_dir, "sample_000", "XRF", "005-xrf_10X-pos_0.bmp")
    # raw_image = io.imread(image_dir)
    #
    # campaign_json_dir = os.path.join(campaign_dir, "config.json")
    # with open(campaign_json_dir, 'r') as json_data:
    #     campaign_data = json.load(json_data)
    # probe_locations = campaign_data['SPECTROMETRY']['OFFSETS']
    #
    # reference_scale = {
    #     'darkfield_px_mm': 3000 / 25,  # 3000 pixels / 25mm
    #     'lightfield_px_mm': 117 / 3,  # 117 pixels / 3mm
    #     'lightfield_x_offset_mm': 1.40,
    #     'lightfield_y_offset_mm': -2.15,
    # }
    #
    # segment_map = fw_image_segmentation(image_directory=image_dir)
    #
    # raw_image_copy = raw_image.copy()
    # masks = probe_mapping(image_matrix=raw_image_copy,
    #                       probe_locations=probe_locations,
    #                       scale_px_mm=reference_scale['lightfield_px_mm'],
    #                       x_offset_mm=reference_scale['lightfield_x_offset_mm'],
    #                       y_offset_mm=reference_scale['lightfield_y_offset_mm'],
    #                       probe_x_mm=0.5,
    #                       probe_y_mm=3.0,
    #                       plot=False,
    #                       rotate90=0)
    #
    # homogeneity_score(image_matrix=segment_map,
    #                   masks=masks)

    #########################
    ### Homogeneity Plots ###
    campaign_dir = r'C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\2020-09-22_17-24-46'
    # campaign_dir = r"C:\Users\Ted\Downloads\2020-09-26_08-21-20-20200928T165533Z-001\2020-09-26_08-21-20"
    homogeneity_data_generation(campaign_directory=campaign_dir)

    # df = pd.read_csv(r"C:\Users\Ted\Downloads\2020-09-22_17-24-46-20200923T201800Z-001\samples_homogen.csv")
    # plot_homogeneity(df=df,
    #                  col1='fuel_oxidizer_ratio_realized',
    #                  col2="temperature_realized",
    #                  color='homogeneity',
    #                  label='sample')