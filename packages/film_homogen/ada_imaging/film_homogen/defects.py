from PIL import Image
import numpy as np
from scipy import ndimage
from skimage.util import img_as_ubyte
import matplotlib.pyplot as plt
import os


def read_image(image):
    jpgFile = Image.open(image).convert('L')
    imagePixels = np.asarray(jpgFile)
    return imagePixels


def breakout_matrices(imagePixels):
    whitePixels = imagePixels > 120

    labeled, nr_objects = ndimage.label(whitePixels)
    bins = np.bincount(labeled.ravel())
    max_index = bins.argsort()[-3:][::-1]
    targetMask = labeled == max_index[1]
    filledMask = ndimage.binary_fill_holes(targetMask)

    croppedImage = imagePixels * filledMask
    defectPixels = np.bitwise_and(croppedImage < 180, filledMask)

    good_film = np.where(croppedImage >= 180, 1, 0)

    return filledMask, croppedImage, defectPixels, good_film


def film_quality(image, output=None):
    imagePixels = read_image(image)
    filledMask, croppedImage, defectPixels, good_film = breakout_matrices(imagePixels)

    totalPixels = sum(sum(filledMask))
    areaPercent = (totalPixels * 100) / (imagePixels.shape[0] * imagePixels.shape[1])

    totalDefects = sum(sum(defectPixels))
    defectPercent = (1 - (totalDefects / totalPixels)) * 100

    if output is not None:
        fig, ax = plt.subplots(nrows=2, ncols=2)

        ax[0, 0].imshow(filledMask)
        ax[0, 0].axis('off')
        ax[0, 1].imshow(croppedImage)
        ax[0, 1].axis('off')
        ax[1, 0].imshow(defectPixels)
        ax[1, 0].axis('off')
        ax[1, 1].imshow(imagePixels)
        ax[1, 1].axis('off')
        fig.suptitle(
            "Area of Shape(%):" + "%.4f" % areaPercent + "\n" + "Quality of Film(%): " + "%.4f" % defectPercent,
            fontsize=14)
        plt.savefig(output, bbox_inches='tight')

    return areaPercent, defectPercent


# def square_area(matrix, x_start, x_end, y_start, y_end):
#     sub_matrix = matrix[x_start:x_end, y_start:y_end]
#     print(sub_matrix)
#     area = np.sum(sub_matrix)
#     return area
#
#
# def size_square(matrix, x, y):
#     x_size = 1
#     y_size = 1
#     y_grow = True
#     x_grow = True
#
#     while y_grow:
#         while x_grow:
#             true_area = x_size * y_size
#             area = square_area(matrix, x, x + x_size, y, y + y_size)
#             if true_area == area:
#
#
#             return area
#
#
#
# def largest_homogeneous_area(image):
#     imagePixels = read_image(image)
#     filledMask, croppedImage, defectPixels, good_film = breakout_matrices(imagePixels)
#
#     x_cols, y_cols = good_film.shape
#     biggest_area = {'x_size': 0,
#                     'y_size': 0,
#                     'area': 0,
#                     'x': 0,
#                     'y': 0}
#
#     for x in range(0, x_cols):
#         for y in range(0, y_cols):
#             pixel_val = good_film[x][y]
#             if pixel_val == 1:
#                 print(size_square(good_film, x, y))
#                 return

if __name__ == "__main__":
    # largest_homogeneous_area(
    #     image=r"C:\Users\Ted\Desktop\combustion_images\combustion_synthesis_30072020_2020-07-30_17-50-28_2_0_003-flir_image-pos_0.jpg")

    areaPercent, defectPercent = film_quality(
        image=r"C:\Users\Ted\Desktop\combustion_images\combustion_synthesis_30072020_2020-07-30_17-50-28_2_0_003-flir_image-pos_0.jpg",
        output=r"C:\Users\Ted\Desktop\combustion_synthesis_30072020_2020-07-30_17-50-28_2_0_003-flir_image-pos_0.jpg")

    print(areaPercent, defectPercent)
