pillow
numpy
pandas
scipy
opencv-python
imageio
scikit-image
scikit-learn
matplotlib
mahotas
git+https://gitlab.com/ada-chem/toolbox.git@master#egg=ada_toolbox