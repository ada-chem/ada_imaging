from setuptools import setup

PACKAGE = 'modelling'
NAME = f'ada_imaging.{PACKAGE}'

about = {}
with open(f'ada_imaging/{PACKAGE}/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

INSTALL_REQUIRES = [
    'scipy',
    'pandas',
    'pillow',
    'numpy',
    'matplotlib',
    'tensorflow',
    'git+https://github.com/tensorflow/docs',
    'scikit-image',
    'python-dotenv',
    'plotly',
    'plotly-express',
    'ada_database @ git+https://gitlab.com/ada-chem/ada_database.git@master#egg=ada_database',
]

setup(
    name=NAME,
    version=about["__version__"],
    install_requires=INSTALL_REQUIRES,
    namespace_packages=['ada_imaging'],
    packages=[NAME],
    zip_safe=False,
)
