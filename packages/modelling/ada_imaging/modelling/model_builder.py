import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
import os
import json
from ada_database.configuration import AdaMongo
from PIL import Image, ImageOps
import io
import tensorflow as tf
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten, Dropout
from keras.datasets import mnist
from keras.utils import to_categorical
import numpy as np
import pandas as pd
from numpy import expand_dims
from sklearn.model_selection import train_test_split
from dotenv import load_dotenv
import datetime
import plotly.graph_objects as go
import re
import logging
import sys
import matplotlib.pyplot as plt

try:
    load_dotenv('credentials.json')
except Exception as e:
    print(e)


class LoggingConfig:
    '''
    Defines the logger instance that will be used in all tasks.
    '''
    logger = None

    def __init__(self, class_name):
        self.custom_logging(class_name)

    def custom_logging(self, class_name, level=logging.DEBUG):
        '''
        Define the logging parameters for the mutable class.

        :param level: What level of logging to capture
        :return:
        '''

        logger = logging.getLogger(class_name)
        logger.setLevel(level)

        logger.handlers = []

        format_string = ('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        log_format = logging.Formatter(format_string)

        # Creating and adding the console handler
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(log_format)
        logger.addHandler(console_handler)

        LoggingConfig.logger = logger


class Model:
    @staticmethod
    def create_model(X_train):
        model = Sequential()
        model.add(Conv2D(64, kernel_size=6, activation='relu',
                         input_shape=(X_train.shape[1], X_train.shape[2], X_train.shape[3])))
        model.add(Conv2D(32, kernel_size=3, activation='relu'))
        model.add(Flatten())
        model.add(Dense(1))
        model.compile(optimizer='adam',
                      loss='mse',
                      metrics=['mae'])
        model.summary()
        return model


class MnistModel:
    @staticmethod
    def create_model(X_train):
        model = Sequential()
        # 64 in the first layer and 32 in the second layer are the number of nodes in each layer
        # Kernel size is the size of the filter matrix for our convolution

        model.add(Conv2D(64, kernel_size=3, activation='relu',
                         input_shape=(X_train.shape[1], X_train.shape[2], X_train.shape[3])))
        model.add(Conv2D(32, kernel_size=3, activation='relu'))

        # Flatten serves as a connection between the convolution and dense layers
        model.add(Flatten())

        # We will have 10 nodes in our output layer, one for each possible outcome
        model.add(Dense(10, activation='softmax'))

        # The optimizer controls the learning rate
        #  ‘categorical_crossentropy’ for our loss function. This is the most common choice for classification
        # ‘accuracy’ metric to see the accuracy score on the validation set when we train the model
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        model.summary()
        return model


class DataPreprocessing(LoggingConfig):
    '''
    Performs various manipulations to raw image data to get it ready for deep learning.
    '''

    def __init__(self,
                 images_directory: str,
                 score_file: str = None,
                 processed_data_file: str = None):

        self.logging = LoggingConfig(class_name=self.__class__.__name__)

        self.images_directory = images_directory

        if score_file is None:
            if os.path.isfile(os.path.join(images_directory, 'score.json')):
                self.score_file = os.path.join(images_directory, 'score.json')
            else:
                raise Exception('Score file does not exist!')

        if processed_data_file is None:
            if os.path.isfile(os.path.join(images_directory, 'processed_data.npy')):
                self.processed_data_file = os.path.join(images_directory, 'processed_data.npy')
            else:
                raise Exception('Processed data file does not exist!')

        self.X = None
        self.y = None
        self.X_train = None
        self.y_train = None
        self.X_test = None
        self.y_test = None

    def download_images_db(self,
                           images_directory,
                           query,
                           username,
                           password,
                           address,
                           port,
                           database_name,
                           collection_name,
                           ):
        '''
        Download images from the database

        :param images_directory: Directory where images are saved
        :param query: Image search query
        :param username:
        :param password:
        :param address:
        :param port:
        :param database_name:
        :param collection_name:
        :return:
        '''

        try:
            # Get credentials from environment file
            username = os.getenv('USERNAME')
            password = os.getenv('PASSWORD')
            address = os.getenv('ADDRESS')
            port = os.getenv('PORT')
            database_name = os.getenv('DATABASE_NAME')
            collection_name = os.getenv('COLLECTION_NAME')

        except Exception as e:
            self.logging.logger.warning(e)

        # Make connection to database
        am = AdaMongo(address=address,
                      port=port,
                      username=username,
                      password=password,
                      database_name=database_name,
                      collection_name=collection_name)

        # Make the query to the database
        db, collection, fs = am.connect()
        results = collection.find(query)

        # Download the images to the images directory
        for image_number, result in enumerate(results):
            try:
                file_binary = fs.get(result['file']).read()
                img = Image.open(io.BytesIO(file_binary))
                img.save(os.path.join(images_directory, f"image_{image_number}.jpeg"), "jpeg")

            except Exception as e:
                self.logging.logger.error(e)

    def _process_image(self,
                       image_filepath,
                       image_width=None,
                       image_height=None,
                       image_color='L',
                       augment_image=True):
        '''
        Reads an image from the local directory and processes it various ways.
        :param image_filepath: Filepath to the image to process
        :param image_width: The number of pixels wide the image is to be formatted to.
        :param image_height: The number of pixels high the image is to be formatted to.
        :param image_color: Options are 'L' for greyscale and 'RBG' for color.
        :param augment_image: Images are augmented when True.
        :return: A list of arrays of images
        '''
        # Load image
        img = Image.open(image_filepath)

        # To grayscale: 'L', to RBG: 'RGB'
        try:
            img_col = img.convert(image_color)
        except Exception as e:
            self.logging.logger.error(
                f"{e} - Options are 'L' for greyscale and 'RBG' for color.")
            raise

        # width / height aspect ratio
        aspect = img_col.size[0] / img_col.size[1]

        # Resize image
        if image_width is None and image_height is None:
            img_scaled = img_col

        elif image_width is not None and image_height is None:
            img_scaled = img_col.resize((image_width, int(image_width / aspect)), Image.ANTIALIAS)

        elif image_width is None and image_height is not None:
            img_scaled = img_col.resize((int(aspect * image_height), image_height), Image.ANTIALIAS)

        else:
            img_scaled = img_col.resize((image_width, image_height), Image.ANTIALIAS)

        self.logging.logger.info(
            f"{os.path.basename(image_filepath)} - width: {img_scaled.size[0]} x height: {img_scaled.size[1]}")

        image_matrices_list = []

        if augment_image:

            # Flip images vertically and horizontally. Maintains image shape.
            image = img_scaled
            np_image = np.asarray(image)
            image_matrices_list.append(np_image)

            image_v = ImageOps.flip(image)
            np_image_v = np.asarray(image_v)
            image_matrices_list.append(np_image_v)

            image_vh = ImageOps.mirror(image_v)
            np_image_vh = np.asarray(image_vh)
            image_matrices_list.append(np_image_vh)

            image_h = ImageOps.mirror(image)
            np_image_h = np.asarray(image_h)
            image_matrices_list.append(np_image_h)

        else:
            image_matrices_list.append(img_scaled)

        # Reshape arrays prior to completion
        if image_color == 'L':
            reshaped_arrays = [i.reshape((i.shape[0], i.shape[1], 1)) for i in image_matrices_list]
        else:
            reshaped_arrays = [i.reshape((i.shape[0], i.shape[1], 3)) for i in image_matrices_list]

        return reshaped_arrays

    def _save_processed_data(self, X, y):
        '''
        Save the processed data to a numpy file.
        :param X: Processed Images in a numpy array
        :param y: Processed Target values in a numpy array
        :return:
        '''
        processed_data_dict = dict()
        processed_data_dict['X'] = X
        processed_data_dict['y'] = y
        np.save(self.processed_data_file, processed_data_dict)

    def _create_dataset(self,
                        image_width=None,
                        image_height=None,
                        image_color='L',
                        augment_images=True):
        '''
        Makes two np arrays for the training data and the target.
        :param image_width: The number of pixels wide the image is to be formatted to.
        :param image_height: The number of pixels high the image is to be formatted to.
        :param image_color: Options are 'L' for greyscale and 'RBG' for color.
        :param augment_images: True if the images are to be augmented to create more training data
        :return:
        '''

        # Create a list of all the images in the directory
        images = [image for image in os.listdir(self.images_directory) if image.endswith('.jpeg')]

        # Load in the labels for the files
        try:
            with open(self.score_file) as json_file:
                scores_dict = json.load(json_file)
        except Exception as e:
            raise Exception(e)

        processed_images = []
        image_labels = []
        target_dimensions = 1  # default to single dimension

        # Iterate through each image and process and necessary
        for image in images:

            image_directory = os.path.join(self.images_directory, image)
            score = scores_dict[image]

            if type(score) == list:
                target_dimensions = len(score)

            else:
                target_dimensions = 1

            # Process an image. Will return a list of matrices with the augmentations to the image.
            image_matrix_list = self._process_image(image_filepath=image_directory,
                                                    image_width=image_width,
                                                    image_height=image_height,
                                                    image_color=image_color,
                                                    augment_image=augment_images)

            for processed_image_number, processed_image in enumerate(image_matrix_list):
                processed_images.append(processed_image)

                score = np.array(score).reshape(target_dimensions)
                image_labels.append(score)

                self.logging.logger.info(f"{str(image)}-{str(processed_image_number)} : {str(score)}")

        # Convert list of matrices into a array of matrices
        X = np.asarray(processed_images)
        y = np.asarray(image_labels)

        # Save processed data
        self._save_processed_data(X, y)

        return X, y

    def get_processed_data(self,
                           reprocess=True,
                           image_width=None,
                           image_height=None,
                           image_color='L',
                           augment_images=True
                           ):
        '''

        :param reprocess: False will load the existing numpy processed data file
        :param image_width: The number of pixels wide the image is to be formatted to.
        :param image_height: The number of pixels high the image is to be formatted to.
        :param image_color: Options are 'L' for greyscale and 'RBG' for color.
        :param augment_images: True if the images are to be augmented to create more training data
        :return:
        '''

        # Save processed data to this directory
        processed_data_directory = os.path.join(self.images_directory, self.processed_data_file)

        # Reprocess the images from the image directory
        if not os.path.isfile(processed_data_directory) or reprocess:

            X, y = self._create_dataset(image_width=image_width,
                                        image_height=image_height,
                                        image_color=image_color,
                                        augment_images=augment_images)

        else:
            processed_data_dict = np.load(processed_data_directory, allow_pickle=True)
            X = processed_data_dict[()]['X']
            y = processed_data_dict[()]['y']

        self.X = X
        self.y = y

        return X, y

    def test_train_split(self, X=None, y=None, test_size=0.2, random_state=1):
        '''
        Returns 4 arrays for the test train split
        :param X: Processed input data
        :param y: Processed target data
        :param test_size: The ratio of testing to training in the split
        :param random_state: Random seed to initialize split of the data
        :return: X_train, y_train, X_test, y_test
        '''
        X_y = [(X, 'X'), (y, 'y')]

        for data in X_y:
            if data[0] is None and getattr(self, data[1]) is None:
                Exception(f"Must provide {data[1]} argument")
            elif data[0] is not None and getattr(self, data[1]) is None:
                self.logging.logger.info(f"Updating {data[1]} to use the one provided")
                setattr(self, data[1], data[0])
            elif data[0] is None and getattr(self, data[1]) is not None:
                self.logging.logger.info(f"Using existing {data[1]}")
            else:
                self.logging.logger.info(f"Updating {data[1]} to use the one provided")
                setattr(self, data[1], data[0])

        X_train, X_test, y_train, y_test = train_test_split(self.X,
                                                            self.y,
                                                            test_size=test_size,
                                                            random_state=random_state)

        self.logging.logger.info(f'Shape of X_train: {str(X_train.shape)}')
        self.logging.logger.info(f'Shape of y_train: {str(y_train.shape)}')
        self.logging.logger.info(f'Shape of X_test: {str(X_test.shape)}')
        self.logging.logger.info(f'Shape of y_test: {str(y_test.shape)}')

        self.X_train = X_train
        self.y_train = y_train
        self.X_test = X_test
        self.y_test = y_test

        return X_train, y_train, X_test, y_test


class DeepLearning(LoggingConfig):
    '''
    Class for interacting with a deep learning model
    '''

    def __init__(self,
                 model=None,
                 X_train=None,
                 y_train=None,
                 X_test=None,
                 y_test=None,
                 model_directory=None
                 ):

        self.logging = LoggingConfig(class_name=self.__class__.__name__)

        self.model = model
        self.X_train = X_train
        self.y_train = y_train
        self.X_test = X_test
        self.y_test = y_test

        # Save model to this directory if none is provided
        if model_directory is None:
            model_directory = os.path.dirname(os.path.abspath(__file__))

        self.model_directory = model_directory
        self.history = None
        self.datetime = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M_%S")

    def _test_fitting_model(self):
        '''
        Prior to fitting data, ensure that the model inputs and outputs are congruent with the model structure.
        :return:
        '''
        X_shape = self.X_train.shape
        X_shape = X_shape[1:]
        y_shape = self.y_train.shape
        y_shape = y_shape[1:]

        model_input = self.model._feed_input_shapes[0]
        model_input = model_input[1:]
        model_output = self.model._feed_output_shapes[0]
        model_output = model_output[1:]

        self.logging.logger.info(f'Shape of X: {str(X_shape)}')
        self.logging.logger.info(f'Shape of y: {str(y_shape)}')
        self.logging.logger.info(f'Shape of model input: {str(model_input)}')
        self.logging.logger.info(f'Shape of model_output: {str(model_output)}')

        assert (X_shape == model_input), 'Model input is a different shape than the input data shape.'
        assert (y_shape == model_output), 'Model output is a different shape than the target data shape.'

    def _test_input_data(self, input_data):
        '''
        Prior to making a prediction with the model, test the input and out shapes.
        :param input_data:
        :return:
        '''
        X_shape = input_data.shape
        X_shape = X_shape[1:]

        model_input = self.model._feed_input_shapes[0]
        model_input = model_input[1:]

        assert (X_shape == model_input), 'Model input is a different shape than the input data shape.'

    def fit_model(self,
                  X_train=None,
                  y_train=None,
                  X_test=None,
                  y_test=None,
                  epochs=10,
                  batch_size=1,
                  checkpoints=True,
                  early_stop_patience=None,
                  plot=True):
        '''
        Fits data to a keras model.

        :param X_train: Training input data
        :param y_train: Training output data
        :param X_test: Testing input data
        :param y_test: Testing output data
        :param epochs: Number of cycles to pass the full dataset through the model during training
        :param batch_size: How many samples to group togther during an epoch.
        :param checkpoints: Save the model weights at the end of every epoch.
        :param plot: Create a plot of the model performance
        :return:
        '''
        # Update and set attributes
        test_train = [(X_train, 'X_train'), (y_train, 'y_train'), (X_test, 'X_test'), (y_test, 'y_test')]
        for data in test_train:
            if data[0] is None and getattr(self, data[1]) is None:
                Exception(f"Must provide {data[1]} argument")
            elif data[0] is not None and getattr(self, data[1]) is None:
                self.logging.logger.info(f"Updating {data[1]} to use the one provided")
                setattr(self, data[1], data[0])
            elif data[0] is None and getattr(self, data[1]) is not None:
                self.logging.logger.info(f"Using existing {data[1]}")
            else:
                self.logging.logger.info(f"Updating {data[1]} to use the one provided")
                setattr(self, data[1], data[0])

        # Update the model directory
        self.model_directory = os.path.join(self.model_directory, f"model_{self.datetime}")
        os.makedirs(self.model_directory, exist_ok=True)

        callbacks = []

        if checkpoints:
            checkpoint_path = os.path.join(self.model_directory, 'cp-{epoch:04d}.ckpt')
            checkpoint_directory = os.path.dirname(checkpoint_path)

            # Create a callback that saves the model's weights
            cp_callback = tf.keras.callbacks.ModelCheckpoint(
                filepath=checkpoint_path,
                # save_weights_only=True,
                verbose=True,
                period=1)
            callbacks.append(cp_callback)

        if early_stop_patience is not None:
            # Early stopping stops the training process to reduce overfitting.
            # Early stopping stops when the validation loss stops improving for a period of epochs equal to the patience #
            early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=early_stop_patience)
            callbacks.append(early_stop)

        # Test the model to make sure the inputs and outputs make sense
        self._test_fitting_model()

        try:
            self.history = self.model.fit(self.X_train,
                                          self.y_train,
                                          validation_data=(self.X_test, self.y_test),
                                          epochs=epochs,
                                          batch_size=batch_size,
                                          shuffle=True,
                                          callbacks=callbacks
                                          )
        except Exception as e:
            self.logging.logger.error(e)
            plot = False

        if plot:
            self._fit_plot()

        return self.model

    def save_model(self):
        '''
        Save the model
        :return:
        '''
        save_directory = os.path.join(self.model_directory, f'model_{self.datetime}.h5')
        self.model.save(save_directory)

    def _fit_plot(self):
        '''
        Plots the outcome of the model fitting. Saves as a plotly figure.
        :return:
        '''
        fig = go.Figure()
        hist_dict = dict(self.history.history)
        for metric in hist_dict.keys():

            metric_type = re.sub('val_', '', metric)
            df_col = pd.DataFrame({'performance': hist_dict[metric]})
            df_col['epochs'] = np.arange(len(df_col))

            np.random.seed(abs(hash(metric_type)) % (10 ** 8))
            r = np.random.randint(0, 255)
            g = np.random.randint(0, 255)
            b = np.random.randint(0, 255)

            rgb = f"rgb({r},{g},{b})"

            if metric.startswith('val_'):
                name = f'{metric_type} validation'
                fig.add_trace(go.Scatter(x=df_col['epochs'],
                                         y=df_col['performance'],
                                         name=name,
                                         legendgroup=f"{metric_type}",
                                         line=dict(color=rgb)))
            else:
                name = f'{metric_type} training'
                fig.add_trace(go.Scatter(x=df_col['epochs'],
                                         y=df_col['performance'],
                                         name=name,
                                         legendgroup=f"{metric_type}",
                                         line=dict(color=rgb, dash='dash')))

        fig.update_layout(title='Performance Metrics of Model during Training',
                          xaxis_title='Epoch',
                          yaxis_title='Performance')

        fig.write_html(os.path.join(self.model_directory, 'plot.html'))

    def predict(self, input_data):
        '''
        Make a prediction using the model.
        :param input_data: np array that must match training data
        :return: np array of model predictions
        '''
        self._test_input_data(input_data)
        return self.model.predict(input_data)


def example1():
    # Train model from local images
    dp = DataPreprocessing(images_directory='/Users/teddyhaley/Desktop/images')

    dp.get_processed_data(reprocess=True,
                          image_height=100,
                          image_color='L',
                          augment_images=True)

    X_train, y_train, X_test, y_test = dp.test_train_split()

    # Scale the scores so the loss function is more effective.
    y_train, y_test = y_train * 100, y_test * 100

    # Create the keras model instance
    model = Model.create_model(X_train=X_train)

    dl = DeepLearning(X_train=X_train,
                      X_test=X_test,
                      y_train=y_train,
                      y_test=y_test,
                      model=model)

    dl.fit_model(epochs=5,
                 checkpoints=False,
                 early_stop_patience=2)

    dl.save_model()
    print(dl.predict(X_test))


def example2():
    # Train model from MSNIST dataset
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    X_train = X_train.reshape(60000, 28, 28, 1)
    X_test = X_test.reshape(10000, 28, 28, 1)

    # ‘one-hot-encode’: sixth number in our array will have a 1 and the rest of the array will be filled with 0
    y_train = to_categorical(y_train)
    y_test = to_categorical(y_test)

    dl = DeepLearning(X_train=X_train,
                      y_train=y_train,
                      X_test=X_test,
                      y_test=y_test,
                      model=MnistModel().create_model(X_train=X_train))

    dl.fit_model(epochs=5, checkpoints=False)
    dl.save_model()
    print(dl.predict(X_test))


if __name__ == '__main__':
    example1()
