import os
import pandas as pd
import numpy as np

import io as iio
from ada_database.configuration import AdaMongo
from PIL import Image
import logging

from skimage.util import img_as_float
from skimage.util import img_as_ubyte
from skimage import io
from skimage.color import rgb2gray
import mahotas.features.texture as mht
from skimage.segmentation import felzenszwalb

from ada_toolbox.data_analysis_lib import conductivity_analysis as cond
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import plotly.express as px
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split

# Estimators
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neural_network import MLPClassifier

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

credentials = {"username": "ada_admin",
               "password": "Unicorn112!",
               "address": "206.12.90.248",
               "port": "27017",
               "database_name": "ada_database",
               "collection_name": "ada"}

am = AdaMongo(**credentials)
db, collection, fs = am.connect()


def download_images(experiments=[],
                    campaigns=[],
                    samples=[],
                    annealed=None,
                    save_directory=r'C:\Users\Ted\Desktop\combustion_images'):
    query = [{'measurement_type': {'$eq': 'image'}}]
    query += [{'file_type': {'$eq': 'jpg'}}]

    # setup query from inputs
    if len(experiments) > 0:
        query += [{'$and': [{'experiment': {'$eq': x}} for x in experiments]}]

    if len(campaigns) > 0:
        query += [{'$and': [{'campaign': {'$eq': x}} for x in campaigns]}]

    if len(samples) > 0:
        query += [{'$and': [{'sample': {'$eq': x}} for x in samples]}]

    if annealed is not None:
        query += [{'$and': [{'metadata.annealed': {'$eq': annealed}}]}]

    result = collection.find({'$and': query})

    for r in result:

        try:
            file_binary = fs.get(r['file']).read()
            img = Image.open(iio.BytesIO(file_binary))

            filename = f"{r['experiment']}_{r['campaign']}_{r['sample']}_{r['measurement']}_{r['file_name']}.jpg"

            if save_directory is None:
                save_directory = os.path.join(os.path.expanduser("~"), 'Desktop')

            file_directory = os.path.join(save_directory, filename)
            img.save(file_directory)

        except Exception as e:
            logging.warning(e)


def download_conductivity(experiments=[],
                          campaigns=[],
                          samples=[],
                          save_directory=r'C:\Users\Ted\Desktop\combustion_images'):
    query = [{'measurement_type': {'$eq': 'conductivity'}}]

    # setup query from inputs
    if len(experiments) > 0:
        query += [{'$and': [{'experiment': {'$eq': x}} for x in experiments]}]

    if len(campaigns) > 0:
        query += [{'$and': [{'campaign': {'$eq': x}} for x in campaigns]}]

    if len(samples) > 0:
        query += [{'$and': [{'sample': {'$eq': x}} for x in samples]}]

    result = collection.find({'$and': query})
    df = pd.DataFrame(list(result))
    df.to_csv(os.path.join(save_directory, 'conductivity_data.csv'))


def label_image_features(images_directory=r'C:\Users\Ted\Desktop\combustion_images'):
    images = [img for img in os.listdir(images_directory) if img.endswith('.jpg') and not img.endswith('mask.jpg')]

    sample_textures = []

    for image_filename in images:

        try:
            campaign = '_'.join(image_filename.replace('combustion_synthesis_30072020_', "").split('_')[:2])
            sample = int(image_filename.replace('combustion_synthesis_30072020_', "").split('_')[2])
            measurement = int(image_filename.replace('combustion_synthesis_30072020_', "").split('_')[3])

            image_directory = os.path.join(images_directory, image_filename)
            image = io.imread(image_directory)

            # Convert to greyscale for GLCM texture mapping
            image_grey = rgb2gray(image)
            image_grey = img_as_ubyte(image_grey)

            # Use left half of image where deposit is made and 1/8th from top and bottom
            image_grey = image_grey[image_grey.shape[0] // 8:-image_grey.shape[0] // 8, :image_grey.shape[1] // 2]

            # for segment_number, segment in enumerate(tiles):
            features = mht.haralick(image_grey, return_mean=True)
            sample_features = {
                'file': image_filename,
                'campaign': campaign,
                'sample': sample,
                'measurement': measurement,
                'image_directory': image_directory
            }

            for feature_number, feature in enumerate(features):
                sample_features[f"feature_{feature_number}"] = feature

            sample_textures.append(sample_features)

        except Exception as e:
            print(e)

    features_df = pd.DataFrame(sample_textures)
    features_df_filename = 'image_features.csv'
    features_df_directory = os.path.join(images_directory, features_df_filename)
    features_df.to_csv(features_df_directory)


def generate_conductance():
    conductivity_df = pd.read_csv(r"C:\Users\Ted\Desktop\combustion_images\conductivity_data.csv")
    unique_campaigns = conductivity_df['campaign'].unique()
    measurement_conductivity = []
    for campaign in unique_campaigns:
        campaign_df = conductivity_df[conductivity_df['campaign'] == campaign]
        unique_samples = campaign_df['sample'].unique()
        for sample in unique_samples:
            sample_df = campaign_df[campaign_df['sample'] == sample]
            unique_measurements = sample_df['measurement'].unique()
            for measurement in unique_measurements:
                measurement_df = sample_df[sample_df['measurement'] == measurement]
                measurement_df = measurement_df[['Current', 'Voltage', 'campaign', 'sample', 'measurement']]

                # fit resistance line
                r2, slope, intercept = cond.linear_regression_RANSAC(data=measurement_df,
                                                                     x_axis="Current",
                                                                     y_axis="Voltage")

                # check intercept is near 0
                if slope != 0:
                    thresh = 1  # volts
                    if abs(intercept) > thresh:
                        slope = 0

                # convert to conductance
                try:
                    conductance = 1 / slope
                except ZeroDivisionError:
                    conductance = 0

                measurement_dict = {
                    'campaign': measurement_df.iloc[0]['campaign'],
                    'sample': measurement_df.iloc[0]['sample'],
                    'measurement': measurement_df.iloc[0]['measurement'],
                    'conductance': conductance
                }

                measurement_conductivity.append(measurement_dict)

    conductance_df = pd.DataFrame(measurement_conductivity)
    conductance_df.to_csv(r'C:\Users\Ted\Desktop\combustion_images\conductance.csv')
    return conductance_df


def join_images_conductance(images_features_dir=r"C:\Users\Ted\Desktop\combustion_images\image_features.csv",
                            conductance_dir=r'C:\Users\Ted\Desktop\combustion_images\conductance.csv'):
    image_features_df = pd.read_csv(images_features_dir)
    try:
        conductance_df = pd.read_csv(conductance_dir)
    except:
        conductance_df = generate_conductance()

    grouped_conductance_df = conductance_df.groupby(['campaign', 'sample'], as_index=False).agg(
        {'conductance': ['mean', 'std']})

    grouped_conductance_df.columns = grouped_conductance_df.columns.map('_'.join).str.strip('_')
    image_features_df = image_features_df[image_features_df['measurement'] == 0]
    del image_features_df['measurement']

    df = pd.merge(grouped_conductance_df, image_features_df, how='left', on=['campaign', 'sample'])
    df = df.dropna()
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    df = df.reset_index(drop=True)
    return df


def create_PCA_df(df, plot=False):
    # Separating out the features
    df_features = df.loc[:, df.columns.str.contains('^feature|conductance')]
    x = df_features.loc[:, df_features.columns.str.contains('^feature')].values

    # Separating out the target
    y = df_features.loc[:, ['conductance_mean']].values

    # Standardizing the features
    x = StandardScaler().fit_transform(x)

    pca = PCA(n_components=13)
    pc = pca.fit_transform(x)

    if plot:
        features = range(pca.n_components_)
        plt.bar(features, pca.explained_variance_ratio_, color='black')
        plt.xlabel('PCA features')
        plt.ylabel('variance %')
        plt.xticks(features)
        plt.show()

    pca_columns = [f"PCA_{i}" for i in range(0, pc.shape[1])]
    pca_df = pd.DataFrame(data=pc, columns=pca_columns)
    return pca_df


def coef_var(x):
    if x['conductance_std'] == 0:
        coef_variation = 0
    else:
        coef_variation = x['conductance_mean'] / x['conductance_std']

    return coef_variation


def main():
    # label_image_features()
    df = join_images_conductance()
    pca_df = create_PCA_df(df, plot=False)
    pca_cond_df = pd.concat([pca_df, df], axis=1)

    # Feature engineer
    # For PCA df
    pca_cond_df['coef_var'] = pca_cond_df.apply(coef_var, axis=1)
    pca_cond_df['conductive'] = np.where(pca_cond_df['conductance_mean'] > 0.5, 1, 0)

    pca_cond_df.to_csv(r'C:\Users\Ted\Desktop\combustion_images\pca_cond.csv')


    y = pca_cond_df[['conductive']].to_numpy()
    X = pca_cond_df[['PCA_1', 'PCA_2']].to_numpy()

    y_14 = pca_cond_df[['conductive']].to_numpy()
    X_14 = pca_cond_df[pca_cond_df.columns[pca_cond_df.columns.str.contains('^feature')]].to_numpy()

    datasets = [(X, y), (X_14, y_14)]

    models = []

    models.append(("LogisticRegression", LogisticRegression()))
    models.append(("SVC", SVC()))
    models.append(("LinearSVC", LinearSVC()))
    models.append(("KNeighbors", KNeighborsClassifier()))
    models.append(("DecisionTree", DecisionTreeClassifier()))
    models.append(("RandomForest", RandomForestClassifier()))
    rf2 = RandomForestClassifier(n_estimators=100, criterion='gini',
                                 max_depth=10, random_state=0, max_features=None)
    models.append(("RandomForest2", rf2))
    models.append(("AdaBoost", AdaBoostClassifier(
        DecisionTreeClassifier(max_depth=1),
        algorithm="SAMME",
        n_estimators=200)))
    models.append(("MLPClassifier", MLPClassifier(solver='lbfgs', random_state=0)))

    seed = 0
    scoring = 'accuracy'
    results = []
    names = []

    for dataset_num, data in enumerate(datasets):
        for name, model in models:
            kfold = model_selection.KFold(n_splits=10, random_state=seed)
            result = model_selection.cross_val_score(model, data[0], data[1], cv=kfold, scoring=scoring)
            names.append(f'{name}_{dataset_num}')
            results.append(result)

    for i in range(len(names)):
        print(names[i], results[i].mean(), results[i].std())

    # # test train split
    # y = pca_cond_df.pop('conductive').to_numpy()
    # X = pca_cond_df[['PCA_1', 'PCA_2']].to_numpy()
    #
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    #
    # print(X_train)
    #
    # print(pca_cond_df)
    # fig = px.scatter(pca_cond_df,
    #                  x="PCA_1",
    #                  y="PCA_2",
    #                  color="conductive",
    #                  color_continuous_scale='Bluered_r',
    #                  hover_data=["campaign", "sample"])
    # fig.show()
    #
    # fig = px.scatter(pca_cond_df, x="PCA_1", y="PCA_2", color="conductance_mean",
    #                  color_continuous_scale='Bluered_r',
    #                  hover_data=["campaign", "sample"])
    # fig.show()
    #
    # fig = px.scatter(pca_cond_df, x="PCA_1", y="PCA_2", color="conductance_std",
    #                  color_continuous_scale='Bluered_r',
    #                  hover_data=["campaign", "sample"])
    # fig.show()
    #
    # fig = px.scatter(pca_cond_df, x="PCA_1", y="PCA_2", color="coef_var",
    #                  color_continuous_scale='Bluered_r',
    #                  hover_data=["campaign", "sample"])
    # fig.show()

    # fig = px.scatter_3d(pca_cond_df, x="PCA_1", y="PCA_2", z="conductance_mean", color='conductive',
    #                     color_continuous_scale='Bluered_r')
    # fig.show()


if __name__ == "__main__":
    main()
