import cv2
import numpy as np
import os
from pprint import pprint
import matplotlib.pyplot as plt
from skimage import io
from skimage import feature
from skimage.color import rgb2gray
from scipy.signal import savgol_filter
from skimage import data, color
from skimage.transform import hough_circle, hough_circle_peaks
from skimage.feature import canny
from skimage.draw import circle_perimeter


def detect_difference(img1):
    image = io.imread(img1)
    image = rgb2gray(image)
    height, width = image.shape
    y = (image[int(height * 0.7):height, int(width / 3):int(width / 3) + 1]).ravel()
    x = range(0, len(y))

    yhat = savgol_filter(y, len(y) - 1, 2)
    res = abs(y - yhat)
    print(np.var(res))

    plt.plot(x, y)
    plt.plot(x, yhat)
    plt.show()

    # # Draw them
    # fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(10, 4))
    # image = color.gray2rgb(image)
    # for center_y, center_x, radius in zip(cy, cx, radii):
    #     circy, circx = circle_perimeter(center_y, center_x, radius,
    #                                     shape=image.shape)
    #     image[circy, circx] = (220, 20, 20)
    #
    # ax.imshow(image, cmap=plt.cm.gray)
    # plt.show()


def find_circle(img,
                dp=1,
                minDist=4000,
                param1=30,
                param2=50,
                minRadius=1500,
                maxRadius=1800):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.medianBlur(gray, 5)
    circles = cv2.HoughCircles(image=img_blur,
                               method=cv2.HOUGH_GRADIENT,
                               dp=int(dp),
                               minDist=int(minDist),
                               param1=int(param1),
                               param2=int(param2),
                               minRadius=int(minRadius),
                               maxRadius=int(maxRadius))

    if circles is None:
        return False

    else:
        return True


def detect_slide(image,
                 dp=1,
                 minDist=4000,
                 param1=30,
                 param2=30,
                 minRadius=1500,
                 maxRadius=1800
                 ):
    image = io.imread(image)
    return find_circle(image,
                       dp=dp,
                       minDist=minDist,
                       param1=param1,
                       param2=param2,
                       minRadius=minRadius,
                       maxRadius=maxRadius
                       )


def test_1(folder):
    files = [file for file in os.listdir(folder) if file.endswith('.jpg')]

    best_result = 0
    best_params = {}

    param1_range = np.linspace(20, 100.0, num=5)
    param2_range = np.linspace(20, 100.0, num=5)
    # minRadius_range = np.linspace(1500, 1900, num=5)
    # maxRadius_range = np.linspace(1500, 1900, num=5)

    for param1 in param1_range:
        for param2 in param2_range:
            results_sample = []
            for file in files:
                y_test = bool(file.split('_')[0])
                y_pred = detect_slide(os.path.join(folder, file),
                                      param1=param1,
                                      param2=param2)

                if y_test == y_pred:
                    results_sample.append(1)
                else:
                    results_sample.append(0)

            sample_accuracy = np.mean(results_sample)
            if sample_accuracy > best_result:
                best_result = sample_accuracy
                best_params = {
                    'param1': param1,
                    'param2': param2,
                    'accuracy': sample_accuracy
                }

            pprint(best_params)


def test_2(folder,
           param1=50,
           param2=50,
           minRadius=1500,
           maxRadius=1900
           ):
    files = [file for file in os.listdir(folder) if file.endswith('.jpg')]
    results_sample = {}
    for file in files:
        y_test = bool(file.split('_')[0])
        y_pred = detect_slide(os.path.join(folder, file),
                              param1=param1,
                              param2=param2,
                              minRadius=minRadius,
                              maxRadius=maxRadius)
        if y_test == y_pred:
            results_sample[file] = 1
        else:
            results_sample[file] = 0

    pprint(results_sample)


if __name__ == "__main__":
    # test_1("/Users/teddyhaley/Desktop/conc_data")
    # test_2("/Users/teddyhaley/Desktop/conc_data",
    #        param1=30,
    #        param2=30,
    #        minRadius=1500,
    #        maxRadius=1800)

    detect_difference(img1='/Users/teddyhaley/Desktop/conc_data/false_000-blank_flir_images-pos_0.jpg')
    detect_difference(img1='/Users/teddyhaley/Desktop/conc_data/true_00.jpg')
    detect_difference('/Users/teddyhaley/Desktop/conc_data/true_wafer_1.jpg')
