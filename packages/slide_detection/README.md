## Installing

```
pip install "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.slide_detection&subdirectory=packages/slide_detection""
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.slide_detection&subdirectory=packages/slide_detection"
```
