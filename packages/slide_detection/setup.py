from setuptools import setup

PACKAGE = 'slide_detection'
NAME = f'ada_imaging.{PACKAGE}'

about = {}
with open(f'ada_imaging/{PACKAGE}/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

INSTALL_REQUIRES = [
    'pillow',
    'scikit-image',
]

setup(
    name=NAME,
    version=about["__version__"],
    install_requires=INSTALL_REQUIRES,
    namespace_packages=['ada_imaging'],
    packages=[NAME],

    zip_safe=False,
)
