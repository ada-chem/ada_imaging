## Installing

```
pip install "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_thickness&subdirectory=packages/film_thickness""
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_thickness&subdirectory=packages/film_thickness"
```
