from setuptools import setup

PACKAGE = 'film_thickness'
NAME = f'ada_imaging.{PACKAGE}'

about = {}
with open(f'ada_imaging/{PACKAGE}/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

INSTALL_REQUIRES = [
    'scipy',
    'pandas',
    'pillow',
    'numpy',
    'matplotlib',
    'scikit-image',
    'scikit-learn',
]

setup(
    name=NAME,
    version=about["__version__"],
    install_requires=INSTALL_REQUIRES,
    namespace_packages=['ada_imaging'],
    packages=[NAME],
    package_data={"": ["thickness_model.sav", "thickness_model_absorption.sav"]},
    dependency_links=['git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_coverage&subdirectory=packages/film_coverage'],
    include_package_data=True,
    zip_safe=False,
)
