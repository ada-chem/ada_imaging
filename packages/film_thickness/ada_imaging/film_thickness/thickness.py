import os
from skimage import io
from skimage.color import rgb2gray
from ada_imaging.film_coverage import coverage
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pickle


def remove_unanamed(df):
    df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
    return df


def load_model(model_filepath):
    model = pickle.load(open(model_filepath, 'rb'))
    return model


def train_absorption_thickness_model(campaign, thickness_csv):
    thickness_df = pd.read_csv(thickness_csv)
    sample_files = [i for i in os.listdir(campaign) if
                    i.startswith('sample') and os.path.isdir(os.path.join(campaign, i))]

    image_dfs = []

    for sample_file in sample_files:
        sample_path = os.path.join(campaign, sample_file)
        sample_number = int(sample_file.split('_')[1])

        sample_absorption = thickness_df.loc[thickness_df['Sample'] == sample_number, 'Absorption'].values[0]
        blank = io.imread([os.path.join(sample_path, 'IMAGES', i) for i in os.listdir(os.path.join(sample_path, 'IMAGES')) if
                 i.endswith('jpg') and i.startswith('000')][0])
        sample = io.imread([os.path.join(sample_path, 'IMAGES', i) for i in os.listdir(os.path.join(sample_path, 'IMAGES')) if
                  i.endswith('jpg') and i.startswith('001')][0])

        blank_masked, blank_mask = coverage.mask_image(image=blank)
        sample_masked, sample_mask = coverage.mask_image(image=sample, reference_image=blank_masked)

        sample_masked_bw = rgb2gray(sample_masked)
        blank_masked_bw = rgb2gray(blank_masked)

        intensity = (blank_masked_bw - sample_masked_bw) > 0

        intensity_mask_0 = intensity * sample_masked[:, :, 0]
        intensity_mask_1 = intensity * sample_masked[:, :, 1]
        intensity_mask_2 = intensity * sample_masked[:, :, 2]

        r_channel = np.median(intensity_mask_0[intensity_mask_0 > 0])
        g_channel = np.median(intensity_mask_1[intensity_mask_1 > 0])
        b_channel = np.median(intensity_mask_2[intensity_mask_2 > 0])
        gray = np.median(sample_masked_bw[sample_masked_bw > 0])

        image_df = pd.DataFrame({'Sample': [sample_number],
                                 'r_channel': [r_channel],
                                 'g_channel': [g_channel],
                                 'b_channel': [b_channel],
                                 'luminosity': [gray],
                                 'absorption': [sample_absorption]})

        image_dfs.append(image_df)

    df = pd.concat(image_dfs)
    X = df[['r_channel', 'g_channel', 'b_channel', 'luminosity']].to_numpy()
    y = df[['absorption']].to_numpy()

    model = LinearRegression()
    model.fit(X, y)

    filename = 'thickness_model_absorption.sav'
    filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)
    pickle.dump(model, open(filepath, 'wb'))


def get_thickness(sample_image_directory, blank_image_directory, dye_color='red'):
    file_directory = os.path.dirname(os.path.realpath(__file__))

    blank_image = io.imread(blank_image_directory)
    sample_image = io.imread(sample_image_directory)

    blank_masked, blank_mask = coverage.mask_image(image=blank_image)
    sample_masked, sample_mask = coverage.mask_image(image=sample_image, reference_image=blank_masked)

    sample_masked_bw = rgb2gray(sample_masked)
    blank_masked_bw = rgb2gray(blank_masked)

    intensity = (blank_masked_bw - sample_masked_bw) > 0

    intensity_mask_0 = intensity * sample_masked[:, :, 0]
    intensity_mask_1 = intensity * sample_masked[:, :, 1]
    intensity_mask_2 = intensity * sample_masked[:, :, 2]

    r_channel = np.median(intensity_mask_0[intensity_mask_0 > 0])
    g_channel = np.median(intensity_mask_1[intensity_mask_1 > 0])
    b_channel = np.median(intensity_mask_2[intensity_mask_2 > 0])
    gray = np.median(sample_masked_bw[sample_masked_bw > 0])

    if dye_color is None:
        red = np.mean(sample_masked[:, :, 0])
        blue = np.mean(sample_masked[:, :, 2])
        if red > blue:
            dye_color = 'red'
        else:
            dye_color = 'blue'

    if dye_color == 'red':
        print('Using red dye for thickness detection')
        model_filename = 'thickness_model.sav'
        model_directory = os.path.join(file_directory, model_filename)
        model = load_model(model_directory)

        test_data = np.array([r_channel, g_channel, b_channel, gray]).reshape(1, -1)
        prediction = model.predict(test_data)[0][0]

        if prediction <= 0:
            prediction = 0

        return prediction

    elif dye_color == 'blue':
        print('Using blue dye for thickness detection')

        model_filename = 'thickness_model_absorption.sav'
        model_directory = os.path.join(file_directory, model_filename)
        model = load_model(model_directory)

        test_data = np.array([r_channel, g_channel, b_channel, gray]).reshape(1, -1)
        prediction = model.predict(test_data)[0][0]

        if prediction <= 0:
            prediction = 0

        return prediction

    else:
        raise ('Choose either dye_color="blue" or dye_color="red"')


if __name__ == '__main__':
    # train_absorption_thickness_model(campaign='/Users/teddyhaley/Downloads/2020-07-29_14-42-43',
    #                                  thickness_csv='/Users/teddyhaley/Desktop/thickness.csv')

    campaign = '/Users/teddyhaley/Downloads/2020-07-29_14-42-43'
    thickness_csv = '/Users/teddyhaley/Desktop/thickness.csv'
    thickness_df = pd.read_csv(thickness_csv)

    sample_files = [i for i in os.listdir(campaign) if
                    i.startswith('sample') and os.path.isdir(os.path.join(campaign, i))]

    for sample_file in sample_files:
        sample_path = os.path.join(campaign, sample_file)
        sample_number = int(sample_file.split('_')[1])

        sample_absorption = thickness_df.loc[thickness_df['Sample'] == sample_number, 'Absorption'].values[0]
        blank = [os.path.join(sample_path, 'IMAGES', i) for i in os.listdir(os.path.join(sample_path, 'IMAGES')) if
                 i.endswith('jpg') and i.startswith('000')][0]
        sample = [os.path.join(sample_path, 'IMAGES', i) for i in os.listdir(os.path.join(sample_path, 'IMAGES')) if
                  i.endswith('jpg') and i.startswith('001')][0]

        thickness = get_thickness(sample_image_directory=sample,
                            blank_image_directory=blank,
                            dye_color='blue')

        print('Sample:', sample)
        print('Actual:', sample_absorption)
        print('Predicted:', thickness)
        print('Error abs:', abs(thickness - sample_absorption))
        print('Error percent:', 100 * abs(thickness - sample_absorption) / sample_absorption, '%')
        print('\n')