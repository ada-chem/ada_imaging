## Installing

```
pip install "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_coverage&subdirectory=packages/film_coverage""
```

## Upgrading

```
pip install --upgrade --force-reinstall --no-dependencies "git+https://gitlab.com/ada-chem/ada_imaging.git#egg=ada_imaging.film_coverage&subdirectory=packages/film_coverage"
```
