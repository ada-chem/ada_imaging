from skimage import io
from skimage.morphology import disk
from skimage.filters import median
from skimage.morphology import dilation
from skimage.util import img_as_ubyte
from skimage.color import rgb2gray
from skimage.transform import resize
import matplotlib.pyplot as plt
import numpy as np
import os
import cv2


def find_circle(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.medianBlur(gray, 5)
    circles = cv2.HoughCircles(image=img_blur,
                               method=cv2.HOUGH_GRADIENT,
                               dp=1,
                               minDist=4000,
                               param1=30,
                               param2=30,
                               minRadius=1500,
                               maxRadius=1800)

    # Draw slide outline on the mask and fill
    circles = np.uint16(np.around(circles))

    x = int(circles[0, 0][0])
    y = int(circles[0, 0][1])
    r = int(circles[0, 0][2])

    return x, y, r


def make_mask_with_circle(image, x, y, r):
    height, width, channels = image.shape

    # Black image with dimensions as original image to create mask
    mask = np.zeros((height, width, 3), np.uint8)

    # draw the circle
    cv2.circle(mask, (x, y), r, (255, 255, 255), -1)
    return mask


def make_mask_with_handler(mask):
    height, width, channels = mask.shape
    gray_mask = rgb2gray(mask)
    complete_mask = gray_mask[:]
    for i in range(height):
        for j in range(width):
            if gray_mask[i][j] > 0.1:
                complete_mask[i][j] = 1
            else:
                complete_mask[i][j] = 0
    complete_mask = complete_mask.astype('uint8') * 255
    complete_mask = cv2.cvtColor(complete_mask, cv2.COLOR_GRAY2RGB)
    return complete_mask


def crop_image_with_circle(image, x, y, r):
    height, width, channels = image.shape

    x1 = x - r
    if x1 < 0:
        x1 = 0

    x2 = x + r
    if x2 > width:
        x2 = width

    y1 = y - r
    if y1 < 0:
        y1 = 0

    y2 = y + r
    if y2 > height:
        y2 = height

    image = image[y1:y2, x1:x2]
    return image


def mask_image(image, reference_image=None):
    x, y, r = find_circle(image)

    mask = make_mask_with_circle(image, x, y, r)

    cropped_mask = crop_image_with_circle(mask, x, y, r)
    cropped_image = crop_image_with_circle(image, x, y, r)

    cropped_mask_0 = cropped_mask > 0
    circle_mask = cropped_image * cropped_mask_0

    complete_mask = make_mask_with_handler(circle_mask)
    complete_mask_0 = complete_mask > 0
    cropped_image = cropped_image * complete_mask_0

    if reference_image is not None:
        r_x, r_y, r_d = reference_image.shape
        cropped_image = resize(cropped_image, (r_x, r_y))
        complete_mask = resize(complete_mask, (r_x, r_y))

        cropped_image = img_as_ubyte(cropped_image)
        complete_mask = img_as_ubyte(complete_mask)

    return cropped_image, complete_mask


def sample_location(image, x, y, bound=10):
    x1 = int(x - bound)
    x2 = int(x + bound)
    y1 = int(y - bound)
    y2 = int(y + bound)
    image_slice = image[y1:y2, x1:x2]
    return image_slice


def get_coverage(blank_image_path, sample_image_path, sample, measurement, processed_data_directory, file_name=None):
    r_i = io.imread(blank_image_path)
    t_i = io.imread(sample_image_path)

    r_i_masked, r_i_mask = mask_image(image=r_i)
    t_i_masked, t_i_mask = mask_image(image=t_i, reference_image=r_i_masked)

    r_bw = rgb2gray(r_i_masked)
    t_bw = rgb2gray(t_i_masked)

    diff = (r_bw - t_bw) > 0

    diff = median(diff, disk(4))
    diff = dilation(diff, disk(10))

    reference_pixels = np.sum(r_bw[:, :] > 0)
    sample_pixels = np.sum(diff)

    coverage = sample_pixels / reference_pixels
    if coverage > 1:
        coverage = 1

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))
    axes.ravel()[0].imshow(diff)
    axes.ravel()[0].set_title(f"Coverage: {np.round(coverage * 100, 2)}%")
    axes.ravel()[1].imshow(t_i)
    axes.ravel()[1].set_title(f"Image of Sample")
    plt.tight_layout()

    if file_name is None:
        file_name = f"processed_image_s{sample}_m{measurement}.jpg"

    processed_image_path = os.path.join(processed_data_directory, file_name)
    plt.savefig(processed_image_path)
    plt.close()

    return coverage


if __name__ == "__main__":

    files = [file for file in os.listdir('/Users/teddyhaley/Desktop/conc_data') if file.endswith('.jpg') and file.startswith('true')]
    for file_num, file in enumerate(files):
        print(file)
        get_coverage(
            blank_image_path='/Users/teddyhaley/Desktop/conc_data/true_00.jpg',
            sample_image_path=os.path.join('/Users/teddyhaley/Desktop/conc_data/', file),
            sample=0,
            measurement=file_num,
            processed_data_directory='/Users/teddyhaley/Desktop/processed/',
            file_name=file)

