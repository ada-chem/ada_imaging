import os
import pandas as pd
import numpy as np
import json
from pprint import pprint
import collections

# Image processing and segmentation
from skimage.util import img_as_float
from skimage.util import img_as_ubyte
from skimage import io
from skimage.color import rgb2gray
from skimage import exposure
from skimage.segmentation import felzenszwalb, slic
from skimage.filters import gaussian
from skimage.morphology import disk
from skimage.morphology import dilation
from scipy import ndimage
from scipy.ndimage import measurements

import mahotas.features.texture as mht
import pickle

from timeit import default_timer as timer

# clustering
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN, KMeans

# plotting
import matplotlib.pyplot as plt

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)


def mask_brightfield(image, plot=False):
    # Load image
    grayscale = rgb2gray(image)

    # Smooth image
    smooth = gaussian(grayscale, 5)

    # increase contrast of image for easier separation
    logarithmic_corrected = exposure.adjust_log(smooth, 0.5)
    separate_regions = np.where(logarithmic_corrected < 0.3, 1, 0)

    # Dilate covered region to eliminiate small holes
    selem = disk(6)
    eroded = dilation(separate_regions, selem)

    # Remove background and small deformities/shadows
    labeled_array, num_features = measurements.label(eroded)

    background_label = int(
        np.median([labeled_array[0, 0], labeled_array[0, -1], labeled_array[-1, 0], labeled_array[-1, -1]]))

    labelled_list = labeled_array.ravel().tolist()
    occurence_dict = dict(collections.Counter(labelled_list))
    occurence_dict = {key: val for key, val in occurence_dict.items() if val > 15000}
    try:
        del occurence_dict[background_label]
    except:
        pass

    mask = np.zeros_like(grayscale)
    for key in occurence_dict.keys():
        segment_bool = (labeled_array == key)
        mask[segment_bool] = 1

    mask = ndimage.binary_fill_holes(mask).astype(int)

    if plot:
        plt.imshow(mask)
        plt.show()

    return mask


def from_one_hot_to_rgb(incoming, size):
    scale = int(255 / size)
    rgb = [int(scale * incoming), 0, int(scale * incoming)]
    return rgb


def draw_probe_location(image_matrix: np.ndarray,
                        masks: list,
                        conductance_df: pd.DataFrame,
                        sample_number: int,
                        plot: bool = False):
    if len(image_matrix.shape) == 2:

        # image_matrix = gray2rgb(image_matrix)

        categories = np.unique(image_matrix)
        size = len(categories)
        cat_rgb = {}
        for category in categories:
            rgb = from_one_hot_to_rgb(category, size)
            cat_rgb[category] = rgb

        rgb_img = np.zeros((*image_matrix.shape, 3))

        for key in cat_rgb.keys():
            rgb_img[image_matrix == key] = cat_rgb[key]

        image_matrix = rgb_img
        print(image_matrix)

    # image_matrix = img_as_float(image_matrix)
    sample_conductance_df = conductance_df[conductance_df['sample'] == sample_number]

    for position_number, mask in enumerate(masks):
        position_conductance_df = sample_conductance_df[sample_conductance_df['position'] == position_number]
        position_conductance = position_conductance_df.iloc[0]['conductance']

        mask_color = [255, 0, 0]

        if position_conductance > 0:
            mask_color = [0, 255, 0]

        image_matrix[np.nonzero(mask)] = mask_color

    if plot:
        plt.imshow(image_matrix)
        plt.show()

    return image_matrix


def probe_mapping(image_matrix: np.ndarray,
                  probe_locations: list,
                  scale_px_mm: float,
                  reference_box_margin: int = 10,
                  x_offset_mm: float = 0.0,
                  y_offset_mm: float = 0.0,
                  probe_x_mm: float = 3.0,
                  probe_y_mm: float = 0.5,
                  rotate90: int = 0,
                  plot=False):
    for x in range(rotate90):
        image_matrix = np.rot90(image_matrix)

    # Use middle of image as reference
    x = image_matrix.shape[0]
    y = image_matrix.shape[1]
    x_ref = x // 2
    y_ref = y // 2

    # Correct for offset
    x_correction = int(scale_px_mm * x_offset_mm)
    y_correction = int(scale_px_mm * y_offset_mm)

    x_corrected = x_ref + x_correction
    y_corrected = y_ref + y_correction

    # plot the reference points to confirm offset
    if plot:
        matrix_copy = image_matrix.copy()

        # Place center marker
        matrix_copy[x_ref - reference_box_margin: x_ref + reference_box_margin,
        y_ref - reference_box_margin: y_ref + reference_box_margin] = [255, 0, 0]

        matrix_copy[x_corrected - reference_box_margin: x_corrected + reference_box_margin,
        y_corrected - reference_box_margin: y_corrected + reference_box_margin] = [0, 0,
                                                                                   255]
        plt.imshow(matrix_copy)
        plt.show()

    masks = []

    for position_number, mapping in enumerate(probe_locations):

        # Coordinate mapping from robot to slide is inversed
        x_probe_center_uncorrected = mapping['x'] * -1
        y_probe_center_uncorrected = mapping['y'] * -1

        x_probe_center = int(x_corrected + (x_probe_center_uncorrected * scale_px_mm))
        y_probe_center = int(y_corrected + (y_probe_center_uncorrected * scale_px_mm))

        mask_matrix = np.zeros_like(rgb2gray(image_matrix))

        probe_x_margin = int((probe_x_mm * scale_px_mm) // 2)
        probe_y_margin = int((probe_y_mm * scale_px_mm) // 2)

        mask_matrix[x_probe_center - probe_x_margin: x_probe_center + probe_x_margin,
        y_probe_center - probe_y_margin: y_probe_center + probe_y_margin] = 1

        masks.append(mask_matrix)

        if plot:
            probe_matrix = image_matrix.copy()
            print('Position Number: ', position_number)
            probe_matrix[x_probe_center - probe_x_margin: x_probe_center + probe_x_margin,
            y_probe_center - probe_y_margin: y_probe_center + probe_y_margin] = [0, 0, 255]
            plt.imshow(probe_matrix)
            plt.show()

    return masks


def train_clustering_model(images: list,
                           n_clusters=10,
                           segmentation_method='fw',
                           plot=False):

    if segmentation_method != 'fw' and segmentation_method != 'slic':
        print('Segmenation method must be "fw" or "slic".')
        return

    samples_dict = {}
    feature_list = []

    for image_number, image_directory in enumerate(images):

        samples_dict[image_number] = {}
        samples_dict[image_number]['image_directory'] = image_directory

        image = io.imread(image_directory)
        mask = mask_brightfield(image)
        image[mask == 0] = 0

        # Convert image to float
        image = img_as_float(image)
        samples_dict[image_number]['color_image'] = image

        image_grey = rgb2gray(image)
        image_grey = img_as_ubyte(image_grey)
        image_grey[image_grey == 0] = 1
        samples_dict[image_number]['grey_image'] = image_grey

        # segmentation
        if segmentation_method == 'fw':
            raw_segment_map = felzenszwalb(image,
                                           scale=50,
                                           sigma=3,
                                           min_size=50)

        else:
            raw_segment_map = slic(image,
                                   n_segments=1000,
                                   compactness=1,
                                   sigma=4)

        samples_dict[image_number]['raw_segment_map'] = raw_segment_map

        unique_segments = np.unique(raw_segment_map)
        samples_dict[image_number]['segment_number'] = {}

        # Convert segment to bitmap
        for segment_number in unique_segments:

            samples_dict[image_number]['segment_number'][segment_number] = {}

            segment_bool = (raw_segment_map == segment_number)
            segment_mask = segment_bool.astype(int)
            samples_dict[image_number]['segment_number'][segment_number]['mask'] = segment_mask

            masked_image_grey = segment_mask * image_grey

            r = np.median(image[segment_bool, 0])
            g = np.median(image[segment_bool, 1])
            b = np.median(image[segment_bool, 2])

            try:
                features = mht.haralick(masked_image_grey,
                                        ignore_zeros=True,
                                        return_mean=True)
            except:
                features = [np.nan for i in range(0, 13)]

            segment_dict = {
                'image_directory': image_directory,
                'image_number': image_number,
                'segment_number': segment_number,
                'feature_r': r,
                'feature_g': g,
                'feature_b': b
            }

            for feature_number, feature in enumerate(features):
                segment_dict[f"feature_{feature_number}"] = feature

            feature_list.append(segment_dict)

    features_df = pd.DataFrame(feature_list)
    features_df.fillna(features_df.mean(), inplace=True)

    X = features_df.loc[:, features_df.columns.str.contains('^feature')].values

    # Standardizing the features
    X = StandardScaler().fit_transform(X)
    model = KMeans(n_clusters=n_clusters).fit(X)
    filename = f'{segmentation_method}_{n_clusters}.sav'
    pickle.dump(model, open(filename, 'wb'))

    if plot:
        features_df['label'] = model.labels_

        # Segment Map
        for sample_number in samples_dict.keys():

            segment_map = np.zeros_like(samples_dict[sample_number]['grey_image'])
            segment_numbers = samples_dict[sample_number]['segment_number'].keys()

            for segment_number in segment_numbers:
                segment = samples_dict[sample_number]['segment_number'][segment_number]['mask']
                segment_bool = np.array(segment, dtype=bool)
                label_number = features_df[
                    (features_df['segment_number'] == segment_number) & (features_df['image_number'] == sample_number)][
                    'label']

                segment_map[segment_bool] = label_number

            samples_dict[sample_number]['clustered_segment_map'] = segment_map
            plt.imshow(segment_map)
            plt.show()

        return samples_dict


def predict_image_labels(image_directory, segmentation_method='fw', n_clusters=10):

    if segmentation_method != 'fw' and segmentation_method != 'slic':
        print('Segmenation method must be "fw" or "slic".')
        return

    image = io.imread(image_directory)

    mask = mask_brightfield(image)
    image[mask == 0] = 0

    image = img_as_float(image)
    image_grey = rgb2gray(image)

    # Reserve 0 for mask mapping. Will make GLCM possible because of irregularly shaped regions.
    image_grey = img_as_ubyte(image_grey)
    image_grey[image_grey == 0] = 1

    segmentation_directory = os.path.dirname(__file__)
    model_file = os.path.join(segmentation_directory, f'{segmentation_method}_{n_clusters}.sav')
    loaded_model = pickle.load(open(model_file, 'rb'))

    if segmentation_method == 'fw':
        raw_segment_map = felzenszwalb(image,
                                       scale=50,
                                       sigma=3,
                                       min_size=50)

    else:
        raw_segment_map = slic(image,
                               n_segments=1000,
                               compactness=1,
                               sigma=4)

    unique_segments = np.unique(raw_segment_map)

    # Convert segment to bitmap
    segment_list = []
    for segment_number in unique_segments:
        segment = (raw_segment_map == segment_number).astype(int)
        segment_list.append((segment, segment_number))

    segment_textures = []
    for segment_tuple in segment_list:
        segment = segment_tuple[0]
        segment_number = segment_tuple[1]

        # Use the segment to mask the grey image to do haralick texture feature analysis
        masked_image_grey = segment * image_grey

        bool_mask = segment == 1

        r = np.median(image[bool_mask, 0])
        g = np.median(image[bool_mask, 1])
        b = np.median(image[bool_mask, 2])

        try:
            features = mht.haralick(masked_image_grey,
                                    ignore_zeros=True,
                                    return_mean=True)
        except:
            features = [np.nan for i in range(0, 13)]

        segment_dict = {
            'segment_number': segment_number,
            'feature_r': r,
            'feature_g': g,
            'feature_b': b
        }

        for feature_number, feature in enumerate(features):
            segment_dict[f"feature_{feature_number}"] = feature

        segment_textures.append(segment_dict)

    features_df = pd.DataFrame(segment_textures)
    features_df.fillna(features_df.mean(), inplace=True)

    X = features_df.loc[:, features_df.columns.str.contains('^feature')].values

    # Standardizing the features
    X = StandardScaler().fit_transform(X)

    predicted = loaded_model.predict(X)
    features_df['label'] = predicted

    # Segment Map
    segment_map = np.zeros_like(image_grey)
    for segment_tuple in segment_list:
        segment = segment_tuple[0]
        segment_number = segment_tuple[1]
        segment_bool = np.array(segment, dtype=bool)
        label_number = features_df[features_df['segment_number'] == segment_number]['label']
        segment_map[segment_bool] = label_number

    return segment_map


def image_segmentation(image_directory, eps=0.5, segments=100, metric='euclidean', min_samples=5, plot=False):
    image = io.imread(image_directory)
    image_grey = rgb2gray(image)
    image_grey = img_as_ubyte(image_grey)

    M = image_grey.shape[0] // segments
    N = image_grey.shape[1] // segments

    tiles = [image[x:x + M, y:y + N] for x in range(0, image.shape[0], M) for y in
             range(0, image.shape[1], N)]

    segment_features = []
    for tile_number, tile in enumerate(tiles):

        r = np.median(tile[:, :, 0])
        g = np.median(tile[:, :, 1])
        b = np.median(tile[:, :, 2])
        tile = img_as_ubyte(rgb2gray(tile))
        features = mht.haralick(tile, return_mean=True)
        segment_feature = {
            'tile_number': tile_number,
            'feature_r': r,
            'feature_g': g,
            'feature_b': b
        }

        for feature_number, feature in enumerate(features):
            segment_feature[f"feature_{feature_number}"] = feature

        segment_features.append(segment_feature)

    segment_df = pd.DataFrame(segment_features)

    X = segment_df.loc[:, segment_df.columns.str.contains('^feature')].values

    # Standardizing the features
    X = StandardScaler().fit_transform(X)
    db = DBSCAN(eps=eps, metric=metric, n_jobs=-1, min_samples=min_samples).fit(X)

    segment_df['label'] = db.labels_ + 1

    segment_map = np.zeros_like(image_grey)
    segment = 0
    for x in range(0, image_grey.shape[0], M):
        for y in range(0, image_grey.shape[1], N):
            segment_label = segment_df[segment_df['tile_number'] == segment]['label'] + 1
            segment_map[x:x + M, y:y + N] = segment_label
            segment = segment + 1

    if plot:
        plt.imshow(segment_map)
        plt.show()

    return segment_map


if __name__ == "__main__":
    #######################################
    ### Multi Image Segmentation Example ###
    min_samples = [5]
    epss = [0.5]
    segments = [50]
    metrics = ['euclidean']
    samples = ["{0:0=3d}".format(i) for i in range(0, 10)]
    images = [rf'/Users/teddyhaley/Desktop/2020-10-05_11-19-45/sample_{sample}/XRF/000-xrf_10X-pos_0.bmp' for sample in
              samples]

    start = timer()

    train_clustering_model(images, n_clusters=10, segmentation_method='slic')

    end = timer()
    print(end - start)