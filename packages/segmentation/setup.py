from setuptools import setup

PACKAGE = 'segmentation'
NAME = f'ada_imaging.{PACKAGE}'

about = {}
with open(f'ada_imaging/{PACKAGE}/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

INSTALL_REQUIRES = [
    'pillow',
    'numpy',
    'pandas',
    'opencv-python',
    'scipy',
    'imageio',
    'scikit-image',
    'mahotas',
    'scikit-learn',
    'matplotlib',
    'plotly',
    'plotly-express',
]

setup(
    name=NAME,
    version=about["__version__"],
    install_requires=INSTALL_REQUIRES,
    namespace_packages=['ada_imaging'],
    packages=[NAME],
    zip_safe=False,
    package_data={
        '': ['*.pkl', '*.sav'],
    },
    include_package_data=True
)
